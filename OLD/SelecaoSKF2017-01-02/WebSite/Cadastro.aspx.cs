﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SKF.Bfl;
using SKFBfl;
using SKF.Entity;
using SKFEntity;
using System.Net;
using System.Net.Mail;

namespace WebSite
{
    public partial class Cadastro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CarregarDistribuidora();
                lblTitle.Text = Convert.ToString(Session["titleTipe"]);
                // Caso Usuario já esteja cadastrado
                PreencherFormulario();
            }
        }
        private void CarregarDistribuidora()
        {
            var distribuidoraEntity = new DistribuidoraEntity();

            var distribuidoraBfl = new DistribuidoraBfl();

            DdlDistribuidora.DataTextField = "CodeNome";
            DdlDistribuidora.DataValueField = "CodeId";
            DdlDistribuidora.DataSource = distribuidoraBfl.ListarDistribuidoras(distribuidoraEntity);
            DdlDistribuidora.DataBind();
            DdlDistribuidora.Items.Insert(0, new ListItem("Selecione", "-1"));
        }
        private void PreencherFormulario()
        {
            if (Session["UserStatus"] == null)
            {
                DdlDistribuidora.SelectedValue = "-1";
                TxtNome.Text = "";
                DdlSexo.SelectedValue = "0";
                TxtEmail.Text = "";
                TxtFone.Text = "";
                TxtDataNascimento.Text = "";
                TxtCep.Text = "";
                TxtEndereco.Text = "";
                TxtNumero.Text = "";
                TxtBairro.Text = "";
                TxtCidade.Text = "";
                TxtUf.Text = "";
            }
            else
            {
                if (Session["UserStatus"].ToString() != "I")
                {
                    lblTitle.Text = "Dados Atulizado";
                    // Executa o metodo atualizar cadastro
                    DdlDistribuidora.SelectedValue = Session["UserDistribuidora"].ToString();
                    TxtNome.Text = Session["UserName"].ToString();
                    DdlSexo.SelectedValue = Session["UserSexo"].ToString();
                    TxtEmail.Text = Session["UserMail"].ToString();
                    TxtFone.Text = Session["UserTelefone"].ToString();
                    TxtDataNascimento.Text = Session["UserDatadeNascimento"].ToString();
                    TxtCep.Text = Session["UserCep"].ToString();
                    TxtEndereco.Text = Session["UserEndereco"].ToString();
                    TxtNumero.Text = Session["UserNumero"].ToString();
                    TxtBairro.Text = Session["UserBairro"].ToString();
                    TxtCidade.Text = Session["UserCidade"].ToString();
                    TxtUf.Text = Session["UserUF"].ToString();
                }
                else
                {
                    DdlDistribuidora.SelectedValue = Session["UserDistribuidora"].ToString();
                    TxtNome.Text = Session["UserName"].ToString();
                    DdlSexo.SelectedValue = Session["UserSexo"].ToString();
                    TxtEmail.Text = Session["UserMail"].ToString();
                    TxtFone.Text = Session["UserTelefone"].ToString();
                    TxtDataNascimento.Text = Session["UserDatadeNascimento"].ToString();
                    TxtCep.Text = Session["UserCep"].ToString();
                    TxtEndereco.Text = Session["UserEndereco"].ToString();
                    TxtNumero.Text = Session["UserNumero"].ToString();
                    TxtBairro.Text = Session["UserBairro"].ToString();
                    TxtCidade.Text = Session["UserCidade"].ToString();
                    TxtUf.Text = Session["UserUF"].ToString();
                }
            }
        }
        protected void BtnRegistrarClick(Object sender, EventArgs e)
        {
            if (DdlDistribuidora.SelectedValue == "-1")
            {
                LblDistribuidora.Visible = true;
                return;
            }
            else
            {
                LblDistribuidora.Visible = false;
            }
            if (DdlSexo.SelectedValue == "0")
            {
                LblSexo.Visible = true;
            }
            else
            {
                LblSexo.Visible = false;
            }

            if (!ValidaEmail(TxtEmail.Text))
            {
                LblEmail.Visible = true;
                return;
            }
            else
            {
                LblEmail.Visible = false;
            }

            if (ckTerm.Checked == false)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "callModalError", "modalErroRegulamento();", true);
            }
            else
            {
                var userEntity = new UserEntity();
                var userBfl = new UserBfl();

                userEntity.UserId = Convert.ToInt32(Session["UserId"].ToString());
                userEntity.UserDistribuidora = DdlDistribuidora.SelectedValue;
                userEntity.UserName = TxtNome.Text;
                userEntity.UserSexo = DdlSexo.SelectedValue;
                userEntity.UserMail = TxtEmail.Text;
                userEntity.UserTelefone = TxtFone.Text; ;
                userEntity.UserDatadeNascimento = TxtDataNascimento.Text;
                userEntity.UserCep = TxtCep.Text;
                userEntity.UserEndereco = TxtEndereco.Text;
                userEntity.UserNumero = TxtNumero.Text + " " + txtComplemento.Text;
                userEntity.UserBairro = TxtBairro.Text;
                userEntity.UserCidade = TxtCidade.Text;
                userEntity.UserUF = TxtUf.Text;
                userEntity.UserPassword = TxtSenha.Text;
                userEntity.UserStatus = "A"; // Ativo

                if (Session["UserStatus"] == "I")
                {
                    // Executa o metodo alterar
                    userBfl.AtualizarCadastro(userEntity);

                    if (userEntity.Retorno)
                    {
                        Session["UserName"] = userEntity.UserName;
                        Response.Redirect("Default.aspx", false);
                    }
                    else
                    {
                        Response.Redirect("Login.aspx");
                    }
                }
                if (Session["UserStatus"] == "new")
                {

                    userEntity.UserMail = TxtEmail.Text;
                    userBfl.VerificadorUsuario(userEntity);

                    if (userEntity.UserName != "")
                    {

                        Page.ClientScript.RegisterStartupScript(this.GetType(), "callModalEmail", "modalEmail();", true);
                    }
                    else
                    {
                        // Executa o metodo inserir um novo registro no sistema.
                        userBfl.CadastrarUsuario(userEntity);

                        userEntity.UserId = Convert.ToInt32(Session["UserId"].ToString());
                        userEntity.UserDistribuidora = DdlDistribuidora.SelectedValue;
                        userEntity.UserName = TxtNome.Text;
                        userEntity.UserSexo = DdlSexo.SelectedValue;
                        userEntity.UserMail = TxtEmail.Text;
                        userEntity.UserTelefone = TxtFone.Text; ;
                        userEntity.UserDatadeNascimento = TxtDataNascimento.Text;
                        userEntity.UserCep = TxtCep.Text;
                        userEntity.UserEndereco = TxtEndereco.Text;
                        userEntity.UserNumero = TxtNumero.Text + " " + txtComplemento.Text;
                        userEntity.UserBairro = TxtBairro.Text;
                        userEntity.UserCidade = TxtCidade.Text;
                        userEntity.UserUF = TxtUf.Text;
                        userEntity.UserPassword = TxtSenha.Text;
                        userEntity.UserStatus = "A"; // Ativo

                        var _email = new MailMessage();

                        _email.To.Add(new MailAddress("contato@selecaoskf.com.br"));
                        _email.From = new MailAddress("contato@selecaoskf.com.br");
                        _email.Subject = "NOVO CADASTRO";
                        _email.IsBodyHtml = true; //  ativar o html                                                        
                        _email.Body = "<html>" +
                        "<head>" +
                        "<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">" +
                        "<meta name=Generator content=\"Microsoft Word 14 (filtered)\">" +
                        "</head>" +
                        "<body lang=PT-BR>" +
                        "<div style=\"width: 500px;\">" +
                        "<p class=MsoNormal>" +
                        "<strong>Distribuidora: </strong>" + DdlDistribuidora.SelectedValue + "<br>" +
                        "<strong>Nome: </strong>" + TxtNome.Text + "<br>" +
                        "<strong>E-mail: </strong>" + TxtEmail.Text + "<br>" +
                        "<strong>Telefone: </strong>" + TxtFone.Text + "<br>" +
                        "<strong>Data de nascimento: </strong>" + TxtDataNascimento.Text + "<br>" +
                        "<strong>CEP: </strong>" + TxtCep.Text + "<br>" +
                        "<strong>Endereço: </strong>" + TxtEndereco.Text + "<br>" +
                        "<strong>Nº: </strong>" + TxtNumero.Text + txtComplemento.Text + "<br>" +
                        "<strong>Bairro: </strong>" + TxtBairro.Text + "<br>" +
                        "<strong>Cidade: </strong>" + TxtCidade.Text + "<br>" +
                        "<strong>UF: </strong>" + TxtUf.Text + "<br>" +
                        "</div>" +
                        "</body>" +
                        "</html>";
                        var cliente = new SmtpClient("smtp.selecaoskf.com.br", 587);
                        using (cliente)
                        {
                            cliente.Credentials = new NetworkCredential("contato@selecaoskf.com.br", "Rnct6615");
                            cliente.EnableSsl = false;
                            cliente.Send(_email);
                        }

                        if (userEntity.Retorno)
                        {
                            Session["UserName"] = userEntity.UserName;
                            Response.Redirect("Default.aspx", false);
                        }
                        else
                        {
                            Response.Redirect("Login.aspx");
                        }
                    }
                }
            }
        }
        protected void BtnEnviarClick(Object sender, EventArgs e)
        {
            if (DdlDistribuidora.SelectedValue == "-1")
            {
                LblDistribuidora.Visible = true;
                return;
            }
            else
            {
                LblDistribuidora.Visible = false;
            }
            if (DdlSexo.SelectedValue == "0")
            {
                LblSexo.Visible = true;
            }
            else
            {
                LblSexo.Visible = false;
            }

            if (!ValidaEmail(TxtEmail.Text))
            {
                LblEmail.Visible = true;
                return;
            }
            else
            {
                LblEmail.Visible = false;
            }

            if (ckTerm.Checked == false)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "callModalError", "modalErroRegulamento();", true);
            }
            else
            {
                var userEntity = new UserEntity();

                userEntity.UserId = Convert.ToInt32(Session["UserId"].ToString());
                userEntity.UserDistribuidora = DdlDistribuidora.SelectedValue;
                userEntity.UserName = TxtNome.Text;
                userEntity.UserSexo = DdlSexo.SelectedValue;
                userEntity.UserMail = TxtEmail.Text;
                userEntity.UserTelefone = TxtFone.Text; ;
                userEntity.UserDatadeNascimento = TxtDataNascimento.Text;
                userEntity.UserCep = TxtCep.Text;
                userEntity.UserEndereco = TxtEndereco.Text;
                userEntity.UserNumero = TxtNumero.Text + " " +txtComplemento.Text;
                userEntity.UserBairro = TxtBairro.Text;
                userEntity.UserCidade = TxtCidade.Text;
                userEntity.UserUF = TxtUf.Text;
                userEntity.UserPassword = TxtSenha.Text;
                userEntity.UserStatus = "A"; // Ativo

                var userBfl = new UserBfl();

                // Caso esteja cadastrado e primeiro acesso, atualiza dos dados no sistema
                //if (Session["UserStatus"].ToString() == "I")
                if (userEntity.UserId != 0)
                {
                    // Executa o metodo alterar
                    userBfl.AtualizarCadastro(userEntity);

                    if (userEntity.Retorno)
                    {
                        Session["UserName"] = userEntity.UserName;
                        Response.Redirect("Default.aspx", false);
                    }
                    else
                    {
                        Response.Redirect("Login.aspx");
                    }
                }
                if (userEntity.UserId == 0)
                {
                    userEntity.UserMail = TxtEmail.Text;
                    userBfl.VerificadorUsuario(userEntity);

                    if(userEntity.UserName != "")
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "callModalEmail", "modalEmail();", true);
                    }
                    else
                    {
                        // Executa o metodo inserir um novo registro no sistema.
                        userBfl.CadastrarUsuario(userEntity);

                        userEntity.UserId = Convert.ToInt32(Session["UserId"].ToString());
                        userEntity.UserDistribuidora = DdlDistribuidora.SelectedValue;
                        userEntity.UserName = TxtNome.Text;
                        userEntity.UserSexo = DdlSexo.SelectedValue;
                        userEntity.UserMail = TxtEmail.Text;
                        userEntity.UserTelefone = TxtFone.Text; ;
                        userEntity.UserDatadeNascimento = TxtDataNascimento.Text;
                        userEntity.UserCep = TxtCep.Text;
                        userEntity.UserEndereco = TxtEndereco.Text;
                        userEntity.UserNumero = TxtNumero.Text + " " + txtComplemento.Text;
                        userEntity.UserBairro = TxtBairro.Text;
                        userEntity.UserCidade = TxtCidade.Text;
                        userEntity.UserUF = TxtUf.Text;
                        userEntity.UserPassword = TxtSenha.Text;
                        userEntity.UserStatus = "A"; // Ativo

                        var _email = new MailMessage();

                        _email.To.Add(new MailAddress("contato@selecaoskf.com.br"));
                        _email.From = new MailAddress("contato@selecaoskf.com.br");
                        _email.Subject = "NOVO CADASTRO";
                        _email.IsBodyHtml = true; //  ativar o html                                                        
                        _email.Body = "<html>" +
                        "<head>" +
                        "<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">" +
                        "<meta name=Generator content=\"Microsoft Word 14 (filtered)\">" +
                        "</head>" +
                        "<body lang=PT-BR>" +
                        "<div style=\"width: 500px;\">" +
                        "<p class=MsoNormal>" +
                        "<strong>Distribuidora: </strong>" + DdlDistribuidora.SelectedValue + "<br>" +
                        "<strong>Nome: </strong>" + TxtNome.Text + "<br>" +
                        "<strong>E-mail: </strong>" + TxtEmail.Text + "<br>" +
                        "<strong>Telefone: </strong>" + TxtFone.Text + "<br>" +
                        "<strong>Data de nascimento: </strong>" + TxtDataNascimento.Text + "<br>" +
                        "<strong>CEP: </strong>" + TxtCep.Text + "<br>" +
                        "<strong>Endereço: </strong>" + TxtEndereco.Text + "<br>" +
                        "<strong>Nº: </strong>" + TxtNumero.Text + txtComplemento.Text + "<br>" +
                        "<strong>Bairro: </strong>" + TxtBairro.Text + "<br>" +
                        "<strong>Cidade: </strong>" + TxtCidade.Text + "<br>" +
                        "<strong>UF: </strong>" + TxtUf.Text + "<br>" +
                        "</div>" +
                        "</body>" +
                        "</html>";
                        var cliente = new SmtpClient("smtp.selecaoskf.com.br", 587);
                        using (cliente)
                        {
                            cliente.Credentials = new NetworkCredential("contato@selecaoskf.com.br", "Rnct6615");
                            cliente.EnableSsl = false;
                            cliente.Send(_email);
                        }

                        if (userEntity.Retorno)
                        {
                            Session["UserName"] = userEntity.UserName;
                            Response.Redirect("Default.aspx", false);
                        }
                        else
                        {
                            Response.Redirect("Login.aspx");
                        }
                    }
                }
            }
        }
        protected void DdlDistribuidora_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DdlDistribuidora.SelectedValue == "-1")
            {
                LblDistribuidora.Visible = true;
                return;
            }
            else
            {
                LblDistribuidora.Visible = false;
            }
        }
        public static bool ValidaEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return true;
            }
            bool Valido = false;
            Regex regEx = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$", RegexOptions.IgnoreCase);
            Valido = regEx.IsMatch(email);
            return Valido;
        }

    }
}