﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace WebSite
{
    public partial class Contato : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                TxtNome.Text = "";
                TxtEmail.Text = "";
                TxtFone.Text = "";
                TxtMensagem.Text = "";
                LblEmail.Visible = false;
            }
        }

        protected void BtnEnviarClick(object sender, EventArgs e) 
        {
            LblEmail.Visible = false;

            if (!ValidaEmail(TxtEmail.Text))
            {
                LblEmail.Visible = true;
                return;
            }
            else
            {
                LblEmail.Visible = false;
            }

            var _email = new MailMessage();

            _email.To.Add(new MailAddress("contato@selecaoskf.com.br"));
            _email.From = new MailAddress("contato@selecaoskf.com.br");
            _email.Subject = "Contato Seleção Skf";
            _email.IsBodyHtml = true; //  ativar o html                                                        
            _email.Body = "<html>" +
            "<head>" +
            "<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">" +
            "<meta name=Generator content=\"Microsoft Word 14 (filtered)\">" +
            "</head>" +
            "<body lang=PT-BR>" +
            "<div style=\"width: 500px;\">" +
            "<p class=MsoNormal>" +
            "<strong>Nome: </strong>" + TxtNome.Text.ToUpper() +"<br>" +
            "<strong>Email: </strong>" + TxtEmail.Text + "<br>" +
            "<strong>Telefone: </strong>" + TxtFone.Text + "<br>" +
            "<strong>Mensagem: </strong>" + TxtMensagem.Text + "</p>" +
            "</div>" +
            "</body>" +
            "</html>";
            var cliente = new SmtpClient("smtp.selecaoskf.com.br", 587);
            using (cliente)
            {
                cliente.Credentials = new NetworkCredential("contato@selecaoskf.com.br", "Rnct6615");
                cliente.EnableSsl = false;
                cliente.Send(_email);
            }
            TxtNome.Text = "";
            TxtEmail.Text = "";
            TxtFone.Text = "";
            TxtMensagem.Text = "";
            LblEmail.Visible = false;

            AlertaSucesso.Visible = true;

        }

        public static bool ValidaEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return true;
            }
            bool Valido = false;
            Regex regEx = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$", RegexOptions.IgnoreCase);
            Valido = regEx.IsMatch(email);
            return Valido;
        }

    }
}