// =====================================================================================
// Chamada para executar as mascaras
    function mascara(o,f){
	    v_obj = o;
	    v_fun = f;
	    setTimeout("execmascara()", 100);
	}
// =====================================================================================
//
	function execmascara(){
	    v_obj.value = v_fun(v_obj.value);
	}
		
// =====================================================================================
// Mascara que s� permite digitar n�meros
	function soNumeros(v){
	    return v.replace(/\D/g,"");
	}
// =====================================================================================
// Mascara para Data
	function data(v){
		v=v.replace(/\D/g,"");
		v=v.replace(/^(\d{2})(\d)/,"$1/$2");
	    v=v.replace(/^(\d{2})\/(\d{2})(\d)/,"$1/$2/$3");
		return v;
	}
// =====================================================================================
// Mascara para Telefone
	function telefone(v){
	    v = v.replace(/\D/g,"");
	    v = v.replace(/^(\d\d)(\d)/g,"($1) $2");
	    v = v.replace(/(\d{4})(\d)/,"$1-$2");
	    return v;
	}
// =====================================================================================
// Mascara pra CPF
	function cpf(v){
	    v = v.replace(/\D/g,"");
	    v = v.replace(/(\d{3})(\d)/,"$1.$2");
	    v = v.replace(/(\d{3})(\d)/,"$1.$2");
	    v = v.replace(/(\d{3})(\d{1,2})$/,"$1-$2");
	    return v;
	}
// =====================================================================================
// Mascara para CEP
	function cep(v){
	    v = v.replace(/\D/g,"");
	    v = v.replace(/D/g,"");
	    v = v.replace(/^(\d{5})(\d)/,"$1-$2");
	    return v;
	}
// =====================================================================================
// Mascara para CNPJ
	function cnpj(v){
	    v=v.replace(/\D/g,"");
	    v=v.replace(/^(\d{2})(\d)/,"$1.$2");
	    v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3");
	    v=v.replace(/\.(\d{3})(\d)/,".$1/$2");
	    v=v.replace(/(\d{4})(\d)/,"$1-$2");
	    return v;
	}
// =====================================================================================
// Mascara para Minuscula
	function minusculo(v){
	    v=v.toLowerCase();  
	    return v;
	}
// =====================================================================================
// Mascara para Maiuscula
	function maiusculo(v){
	    v=v.toUpperCase();  
	    return v;
	}
// =====================================================================================
// Mascara para Moeda
    function moeda(v){
        v=v.replace(/\D/g,"")  //permite digitar apenas n�meros
        v=v.replace(/[0-9]{12}/,"INVALIDO")   //limita pra m�ximo 999.999.999,99
        v=v.replace(/(\d{1})(\d{8})$/,"$1.$2")  //coloca ponto antes dos �ltimos 8 digitos
        v=v.replace(/(\d{1})(\d{5})$/,"$1.$2")  //coloca ponto antes dos �ltimos 5 digitos
        v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2")    //coloca virgula antes dos �ltimos 2 digitos
        return v;
    }	
// =====================================================================================
// Mascara para cartao
    function cartao(v){
	    v = v.replace(/\D/g,"");
	    v = v.replace(/(\d{4})(\d)/,"$1 $2");
	    v = v.replace(/(\d{4})(\d)/,"$1 $2");
	    v = v.replace(/(\d{4})(\d)/,"$1 $2");
	    v = v.replace(/(\d{4})(\d)/,"$1 $2");
	    return v;
	}
// =====================================================================================
// Mascara para Telefone com DDD
    function telefoneddd(v){
        v=v.replace(/\D/g,"")                 
        v=v.replace(/^(\d\d)(\d)/g,"($1) $2")    
        return v
    }

// =====================================================================================
//// =====================================================================================
//// Mascara para Moeda
//      function moeda_OLD(v){
//          v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3") //Coloca ponto entre o quinto e o sexto d�gitos
//          return v
//      }
//// =====================================================================================
//// Mascara para Moeda
//      function moeda(v){  
//          z = v.value;
//          z=z.replace(/\D/g,"")  //permite digitar apenas n�meros
//          z=z.replace(/[0-9]{12}/,"inv�lido")   //limita pra m�ximo 999.999.999,99
//          z=z.replace(/(\d{1})(\d{8})$/,"$1.$2")  //coloca ponto antes dos �ltimos 8 digitos
//          z=z.replace(/(\d{1})(\d{5})$/,"$1.$2")  //coloca ponto antes dos �ltimos 5 digitos
//          z=z.replace(/(\d{1})(\d{1,2})$/,"$1,$2")    //coloca virgula antes dos �ltimos 2 digitos
//          v.value = z;
//          return v;
//      }
// =====================================================================================
function MxLength(obj,max){
    if (obj.value.length > max)
    {
        alert('A mensagem de ter no maximo ' + max + ' caracteres.');
        obj.value = obj.value.substring(0,max);
    }    
    return obj;
}