﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="InserirVendas.aspx.cs" Inherits="WebSite.InserirVendas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Distribuidora" />
    <div class="col-md-12 form-group">
        <div class="col-md-4">
            <asp:UpdatePanel ID="updDistribuidora" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="True">
                <ContentTemplate>
                    <asp:DropDownList runat="server" OnSelectedIndexChanged="ddlDistribuidora_OnSelectedIndexChanged" AutoPostBack="True" EnableViewState="True" ID="ddlDistribuidora"></asp:DropDownList>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlDistribuidora" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div class="col-md-3">
            <asp:UpdatePanel ID="updMes" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="True">
                <ContentTemplate>
                    <asp:DropDownList runat="server" ID="ddlMes" OnSelectedIndexChanged="ddlMes_OnSelectedIndexChanged" AutoPostBack="True" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlMes" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div class="modal fade" id="skfSemMeta" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id=""></h4>
                    </div>
                    <div class="modal-body">
                        Não foi possível inserir vendas para este participante porque não foi lançado a meta referênte ao mês!
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <%--<button type="button" class="btn btn-primary"></button>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <asp:UpdatePanel ID="updGridInsert" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="True">
        <ContentTemplate>
            <asp:GridView runat="server" ID="gridInsert" DataKeyField="UserID" AutoGenerateColumns="False" CssClass="table table-bordered" OnRowEditing="gridInsert_OnRowEditing" OnRowUpdating="gridInsert_OnRowUpdating" OnRowCancelingEdit="gridInsert_OnRowCancelingEdit">
                <Columns>
                    <asp:BoundField DataField="UserId" ReadOnly="True" ItemStyle-CssClass="esconde" HeaderStyle-CssClass="esconde" />
                    <asp:BoundField DataField="UserName" HeaderText="Nome" ReadOnly="True" />
                    <asp:BoundField HeaderText="Vendas" DataField="Vendas" />
                    <asp:CommandField ShowDeleteButton="False" ShowEditButton="True" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="gridInsert" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
