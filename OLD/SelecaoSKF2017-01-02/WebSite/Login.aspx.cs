﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SKFEntity;
using SKFBfl;

namespace WebSite
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userEntity = new UserEntity();

            if (Session["UserName"] != null)
            {
                Response.Redirect("Default.aspx");
            }
        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            // Criar uma instancia com do objeto User
            var userEntity = new UserEntity();
            var userBfl = new UserBfl();

            // Passando parametros
            userEntity.UserMail = txtUser.Text;
            userEntity.UserPassword = txtPassword.Text;

            userBfl.PesquisarUsuario(userEntity);

            if (userEntity.UserMail != "" && userEntity.UserPassword != "")
            {
                if(userEntity.UserStatus != "I")
                {
                    Session["UserId"] = userEntity.UserId;
                    Session["UserName"] = userEntity.UserName;
                    Session["UserPerfil"] = userEntity.UserPerfil;
                    Session["UserDistribuidora"] = userEntity.UserDistribuidora;

                    Response.Redirect("Default.aspx", false);
                }
                else 
                {
                    
                        Session["titleTipe"] = "Atualizar dados";
                        // Executa o metodo atualizar cadastro
                        Session["UserId"] = Convert.ToString(userEntity.UserId);
                        Session["UserDistribuidora"] = userEntity.UserDistribuidora.Trim();
                        Session["UserName"] = userEntity.UserName.ToUpper();
                        if (userEntity.UserSexo != "Feminino" && userEntity.UserSexo != "Masculino")
                        {
                            userEntity.UserSexo = "0";
                        }
                        if (userEntity.UserTelefone == "NULL" || userEntity.UserTelefone == "")
                        {
                            userEntity.UserTelefone = "";
                        }
                        if (userEntity.UserDatadeNascimento == "NULL" || userEntity.UserDatadeNascimento == "")
                        {
                            userEntity.UserDatadeNascimento = "";
                        }
                        if (userEntity.UserCep == "NULL" || userEntity.UserCep == "")
                        {
                            userEntity.UserCep = "";
                        }
                        if (userEntity.UserEndereco == "NULL" || userEntity.UserEndereco == "")
                        {
                            userEntity.UserEndereco = "";
                        }
                        if (userEntity.UserNumero == "NULL" || userEntity.UserNumero == "")
                        {
                            userEntity.UserNumero = "";
                        }
                        if (userEntity.UserBairro == "NULL" || userEntity.UserBairro == "")
                        {
                            userEntity.UserBairro = "";
                        }
                        if (userEntity.UserCidade == "NULL" || userEntity.UserCidade == "")
                        {
                            userEntity.UserCidade = "";
                        }
                        if (userEntity.UserUF == "NULL" || userEntity.UserUF == "")
                        {
                            userEntity.UserUF = "";
                        }

                        Session["UserSexo"] = userEntity.UserSexo;
                        Session["UserMail"] = userEntity.UserMail.ToLower();
                        Session["UserTelefone"] = userEntity.UserTelefone;
                        Session["UserDatadeNascimento"] = userEntity.UserDatadeNascimento;
                        Session["UserCep"] = userEntity.UserCep;
                        Session["UserEndereco"] = userEntity.UserEndereco;
                        Session["UserNumero"] = userEntity.UserNumero;
                        Session["UserBairro"] = userEntity.UserBairro;
                        Session["UserCidade"] = userEntity.UserCidade;
                        Session["UserUF"] = userEntity.UserUF;
                        Session["UserPassword"] = userEntity.UserPassword;
                        //Session["UserStatus"] = userEntity.UserStatus;
                        Session["UserStatus"] = "I";


                        // Será redirecionado para completar o preenchimento do formulario com alguns dados e valida.
                        Response.Redirect("Cadastro.aspx");                
                }
            }
            else
            {
                txtPassword.Text = "";
                ScriptManager.RegisterStartupScript(this, GetType(), "callSuport", "divSuport();", true);
                user.Visible = true;
            }
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            Session["UserStatus"] = "new";
            Session["UserId"] = "0";
            Session["titleTipe"] = "Cadastro";
            Session["UserStatus"] = "I";
            Session["UserDistribuidora"] = "";
            Session["UserName"] = "";
            Session["UserSexo"] = "";
            Session["UserMail"] = "";
            Session["UserTelefone"] = "";
            Session["UserDatadeNascimento"] = "";
            Session["UserCep"] = "";
            Session["UserEndereco"] = "";
            Session["UserNumero"] = "";
            Session["UserBairro"] = "";
            Session["UserCidade"] = "";
            Session["UserUF"] = "";
            Response.Redirect("Cadastro.aspx");
        }

    }
}