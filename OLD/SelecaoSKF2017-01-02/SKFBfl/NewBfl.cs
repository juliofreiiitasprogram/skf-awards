﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKFData;
using SKFEntity;

namespace SKFBfl
{
    public class NewBfl
    {
        public List<NewEntity> ListarNoticias(NewEntity newEntity)
        {
            try
            {
                var newData = new NewData();

                return newData.ListarNoticias(newEntity);

            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
