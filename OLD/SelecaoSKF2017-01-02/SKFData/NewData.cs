﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKFEntity;

namespace SKFData
{
    public class NewData : Contexto
    {
        public List<NewEntity> ListarNoticias(NewEntity newEntity)
        {
            var listarNews = new List<NewEntity>();

            Cnn().Open();
            var cmd = new SqlCommand();
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;
            SqlDataReader dr = null;

            sb.Append("SELECT * FROM SKFNoticias");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var newsEntity = new NewEntity();

                    newsEntity.NewsID = Convert.ToInt32(dr["NewsID"]);
                    newsEntity.Titulo = dr["Titulo"].ToString();
                    newsEntity.SubTitulo = dr["SubTitulo"].ToString();
                    newsEntity.Data = dr["Data"].ToString();
                    newsEntity.Hora = dr["Hora"].ToString();
                    newsEntity.Texto = dr["Texto"].ToString();
                    newsEntity.imagem = dr["imagem"].ToString();

                    listarNews.Add(newsEntity);
                }
                return listarNews;
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}
