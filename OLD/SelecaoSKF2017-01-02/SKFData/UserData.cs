﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKFEntity;

namespace SKFData
{
    public class UserData : Contexto
    {
        public UserEntity PesquisarUsuario(UserEntity userEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append("SELECT * FROM USER_REGISTER WHERE UserMail = " + "'" + userEntity.UserMail.Trim() + "'" + " AND UserPassword = " + "'" + userEntity.UserPassword.Trim() + "'");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    userEntity.UserId = Convert.ToInt32(dr["UserId"]);
                    userEntity.UserDistribuidora = dr["UserDistribuidora"].ToString();
                    userEntity.UserName = dr["UserName"].ToString();
                    userEntity.UserSexo = dr["UserSexo"].ToString();
                    userEntity.UserMail = dr["UserMail"].ToString();
                    userEntity.UserTelefone = dr["UserTelefone"].ToString();
                    userEntity.UserDatadeNascimento = dr["UserDatadeNascimento"].ToString();
                    userEntity.UserCep = dr["UserCep"].ToString();
                    userEntity.UserEndereco = dr["UserEndereco"].ToString();
                    userEntity.UserNumero = dr["UserNumero"].ToString();
                    userEntity.UserBairro = dr["UserBairro"].ToString();
                    userEntity.UserCidade = dr["UserCidade"].ToString();
                    userEntity.UserUF = dr["UserUF"].ToString();
                    userEntity.UserStatus = dr["UserStatus"].ToString();
                }
                else
                {
                    userEntity.UserMail = "";
                    userEntity.UserPassword = "";
                }
                return userEntity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }

        }
        public UserEntity VerificadorUsuario(UserEntity userEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append("SELECT * FROM USER_REGISTER WHERE UserMail = '" + userEntity.UserMail.Trim() + "'");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    userEntity.UserId = Convert.ToInt32(dr["UserId"]);
                    userEntity.UserName = dr["UserName"].ToString();
                    userEntity.UserMail = dr["UserMail"].ToString();
                }
                else
                {
                    userEntity.UserId = 0;
                    userEntity.UserName = "";
                    userEntity.UserMail = "";
                    userEntity.UserPassword = "";
                }
                return userEntity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }

        }
        public UserEntity PesquisarUsuarioExiste(UserEntity userEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append("SELECT * FROM USER_REGISTER WHERE UserMail = " + "'" + userEntity.UserMail.Trim() + "'");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    // Usuario cadastrado
                    userEntity.UserId = Convert.ToInt32(dr["UserId"]);
                    userEntity.UserDistribuidora = dr["UserDistribuidora"].ToString();
                    userEntity.UserName = dr["UserName"].ToString();
                    userEntity.UserSexo = dr["UserSexo"].ToString();
                    userEntity.UserMail = dr["UserMail"].ToString();
                    userEntity.UserTelefone = dr["UserTelefone"].ToString();
                    userEntity.UserDatadeNascimento = dr["UserDatadeNascimento"].ToString();
                    userEntity.UserCep = dr["UserCep"].ToString();
                    userEntity.UserEndereco = dr["UserEndereco"].ToString();
                    userEntity.UserNumero = dr["UserNumero"].ToString();
                    userEntity.UserBairro = dr["UserBairro"].ToString();
                    userEntity.UserCidade = dr["UserCidade"].ToString();
                    userEntity.UserUF = dr["UserUF"].ToString();
                    userEntity.UserStatus = dr["UserStatus"].ToString();

                    userEntity.Retorno = true;
                }
                else
                {
                    userEntity.Retorno = false;
                }

                return userEntity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }

        }
        public UserEntity PesquisarUsuarioMeta(UserEntity userEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append("SELECT B.Meta, B.Mes, C.CodeTipo " +
                      "FROM " +
                      "USER_REGISTER      AS A " +
                      "JOIN " +
                      "SKFMeta            AS B " +
                      "ON " +
                      "A.UserID = B.UserID " +
                      "JOIN " +
                      "SKFDistribuidora   AS C " +
                      "ON " +
                      "A.UserDistribuidora = C.CodeID " +
                      "WHERE A.UserID = " + "'" + userEntity.UserId + "' AND B.Mes = '" + userEntity.MesMeta + "'");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    userEntity.Meta = Convert.ToInt32(dr["Meta"]);
                    userEntity.DistribuidoraCodeTipo = (dr["CodeTipo"].ToString());
                    userEntity.Retorno = true;
                }
                else
                {
                    userEntity.Retorno = false;
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }
            return userEntity;
        }
        public UserEntity AtualizarCadastro(UserEntity userEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append(
                "UPDATE USER_REGISTER SET " +
                "UserDistribuidora = '" + userEntity.UserDistribuidora.Trim() +
                "', UserName = '" + userEntity.UserName.ToUpper() +
                "', UserSexo = '" + userEntity.UserSexo +
                "', UserMail = '" + userEntity.UserMail.ToLower() +
                "', UserTelefone = '" + userEntity.UserTelefone +
                "', UserDatadeNascimento = '" + userEntity.UserDatadeNascimento +
                "', UserCep = '" + userEntity.UserCep +
                "', UserEndereco = '" + userEntity.UserEndereco +
                "', UserNumero = '" + userEntity.UserNumero +
                "', UserBairro = '" + userEntity.UserBairro +
                "', UserCidade = '" + userEntity.UserCidade +
                "', UserUF = '" + userEntity.UserUF +
                "', UserPassword = '" + userEntity.UserPassword +
                "', UserStatus = '" + userEntity.UserStatus + "' WHERE UserId = '" + userEntity.UserId + "'");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();

                userEntity.Retorno = true;

                return userEntity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }
        }

        public UserEntity CadastrarUsuario(UserEntity userEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append(
                "INSERT INTO USER_REGISTER (" +
                "UserDistribuidora, " +
                "UserName, " +
                "UserSexo, " +
                "UserMail, " +
                "UserTelefone, " +
                "UserDatadeNascimento, " +
                "UserCep, " +
                "UserEndereco, " +
                "UserNumero, " +
                "UserBairro, " +
                "UserCidade, " +
                "UserUF, " +
                "UserPassword, " +
                "UserStatus) values (" +
                "'" + userEntity.UserDistribuidora.Trim() + "', " +
                "'" + userEntity.UserName.ToUpper() + "', " +
                "'" + userEntity.UserSexo + "', " +
                "'" + userEntity.UserMail.ToLower() + "', " +
                "'" + userEntity.UserTelefone + "', " +
                "'" + userEntity.UserDatadeNascimento + "', " +
                "'" + userEntity.UserCep + "', " +
                "'" + userEntity.UserEndereco + "', " +
                "'" + userEntity.UserNumero + "', " +
                "'" + userEntity.UserBairro + "', " +
                "'" + userEntity.UserCidade + "', " +
                "'" + userEntity.UserUF + "', " +
                "'" + userEntity.UserPassword + "', " +
                "'" + userEntity.UserStatus + "')"
                );
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
               
                userEntity.Retorno = true;
                return userEntity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }
        }
    }
}
