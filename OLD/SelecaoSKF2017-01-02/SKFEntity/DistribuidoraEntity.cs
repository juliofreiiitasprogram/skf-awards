﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKF.Entity
{
    public class DistribuidoraEntity
    {
        public int CodeID { get; set; }
        public string CodeNumero { get; set; }
        public string CodeStatus { get; set; }
        public string CodeNome { get; set; }
        public int MetaJaneiro { get; set; }
        public int MetaFevereiro { get; set; }
        public int MetaMarco { get; set; }
        public int MetaAbril { get; set; }
        public int MetaMaio { get; set; }

    }
}
