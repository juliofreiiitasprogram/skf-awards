﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKFEntity
{
    public class UserEntity
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserMail { get; set; }
        public string UserPassword { get; set; }
        public int UserPerfil { get; set; }
        public string UserCep { get; set; }
        public string UserEndereco { get; set; }
        public string UserDistribuidora { get; set; }
        public DateTime UserTermoAceite { get; set; }
        public DateTime UserTime2 { get; set; }
        public string UserStatus { get; set; }
        public string UserCpf { get; set; }
        public string UserNumero { get; set; }
        public string UserTelefone { get; set; }
        public string UserDatadeNascimento { get; set; }
        public string UserSexo { get; set; }
        public string UserDDD { get; set; }
        public string UserCargo { get; set; }
        public string UserDepartamento { get; set; }
        public string UserBairro { get; set; }
        public string UserCidade { get; set; }
        public string UserUF { get; set; }
        public string UserGerente { get; set; }
        public string UserTermo { get; set; }
        public string UserLastChangePassword { get; set; }
        public int MetaJaneiro { get; set; }
        public int MetaFevereiro { get; set; }
        public int MetaMarco { get; set; }
        public int MetaAbril { get; set; }
        public int MetaMaio { get; set; }
        public int ObjetivoRolamentos { get; set; }
        public int ObjetivoLinha { get; set; }
        public int ObjetivoPolia { get; set; }
        public int ObjetivoDemaisSKF { get; set; }
        public int Meta { get; set; }
        public string DistribuidoraCodeTipo { get; set; }
        public int MesMeta { get; set; }
        public bool Retorno { get; set; }
    }
}
