﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKFData;
using SKFEntity;

namespace SKFBfl
{
    public class UserBfl
    {
        public UserEntity PesquisarUsuario(UserEntity userEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var userData = new UserData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                userData.PesquisarUsuario(userEntity);
                return userEntity;
            }
            catch
            {

                return null;
            }

        }
        public UserEntity PesquisarUsuarioMeta(UserEntity userEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var userData = new UserData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                userData.PesquisarUsuarioMeta(userEntity);
                return userEntity;
            }
            catch
            {

                return null;
            }

        }
    }
}
