﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKFEntity;

namespace SKFData
{
    public class UserData : Contexto
    {
        public UserEntity PesquisarUsuario(UserEntity userEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append("SELECT * FROM USER_REGISTER WHERE UserMail = " + "'" + userEntity.UserMail.Trim() + "'" + " AND UserPassword = " + "'" + userEntity.UserPassword.Trim() + "'" + "AND UserStatus = 'A'");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    userEntity.UserId = Convert.ToInt32(dr["UserID"]);
                    userEntity.UserMail = (dr["UserMail"].ToString());
                    userEntity.UserPerfil = Convert.ToInt32(dr["UserPerfil"]);
                    userEntity.UserPassword = (dr["UserPassword"].ToString());
                    userEntity.UserName = (dr["UserName"].ToString());
                    userEntity.UserDistribuidora = (dr["UserDistribuidora"].ToString());
                }
                else
                {
                    userEntity.UserMail = "";
                    userEntity.UserPassword = "";
                }
                return userEntity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }

        }
        public UserEntity PesquisarUsuarioMeta(UserEntity userEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append("SELECT B.Meta, B.Mes, C.CodeTipo " +
                      "FROM " +
                      "USER_REGISTER      AS A " +
                      "JOIN " +
                      "SKFMeta            AS B " +
                      "ON " +
                      "A.UserID = B.UserID " +
                      "JOIN " +
                      "SKFDistribuidora   AS C " +
                      "ON " +
                      "A.UserDistribuidora = C.CodeID " +
                      "WHERE A.UserID = " + "'" + userEntity.UserId + "' AND B.Mes = '" + userEntity.MesMeta + "'");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    userEntity.Meta = Convert.ToInt32(dr["Meta"]);
                    userEntity.DistribuidoraCodeTipo = (dr["CodeTipo"].ToString());
                    userEntity.Retorno = true;
                }
                else
                {
                    userEntity.Retorno = false;
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }
            return userEntity;
        }        
    }
}
