﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Regulamento.aspx.cs" Inherits="WebSite.Regulamento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div id="regulamento" class="col-xs-12 space simple-page">
            <div class="head">
                <div class="title">Regulamento</div>
            </div>
            <div class="content">
                <div class="body">
                    <div class="post-content regulamento-items">
                        <div class="regulamento-item">
                            <%--<h2><span class="number">1.0</span><span class="text"> A campanha</span></h2>--%>
                            <h3><span class="number">1.0</span><span class="text"> A campanha</span><a href="#regulamento-10-10" class="toggle pull-right" role="button" data-toggle="collapse" aria-controls="regulamento-10-10"><i class="fa fa-minus-square-o"></i></a></h3>
                            <div id="regulamento-10-10" class="collapse in" aria-expanded="true">
                                <p>1.1 A campanha Seleção SKF 2017 vai destacar os profissionais da área de vendas da rede distribuidores industriais e automotivos da SKF do Brasil.</p>
                                <br />
                                <p>1.2 São elegíveis a participar da Campanha os vendedores e gerentes de vendas dos distribuidores que preencherem seu cadastro no site www.selecaoskf.com.br , até o dia 31 de agosto de 2016, e que também trabalhem em:</p>
                                <p style="padding-left: 5%;">1.2.1 Um distribuidor autorizado ou distribuidor CMP Industrial que comprou no mínimo R$ 1,4 milhão de NET Sales em produtos SKF no período de janeiro a junho de 2016.</p>
                                <p style="padding-left: 5%;">1.2.2 Um distribuidor automotivo que comprou no mínimo R$ 300.000,00 por mês, de produtos SKF no período de janeiro a julho de 2016.</p>
                                <br />
                                <p>1.3 A Campanha Seleção SKF será realizada no período de 01/08/2016 a 30/11/2016.</p>
                                <p style="padding-left: 5%;">1.3.1 Para os Distribuidores Industriais, estamos considerando também, para cálculo do resultado, os valores vendidos em produtos SKF no mês de julho de 2016.</p>
                                <br />
                                <p>1.4 Serão premiados na campanha:</p>
                                <p style="padding-left: 5%;">1.4.1 O melhor vendedor por distribuidor autorizado ou distribuidor CMP industrial, que tiver reunido o maior número de pontos durante o período de apuração da campanha, sendo considerado um (1) vendedor para cada distribuidor;</p>
                                <p style="padding-left: 5%;">1.4.2 O gerente de filial de distribuidor automotivo que tiver reunido o maior número de pontos durante o período de apuração, sendo considerado um (1) gerente para cada distribuidor;</p>
                                <p style="padding-left: 5%;">1.4.3 O vendedor destaque de toda a distribuição industrial será aquele que, ao final da campanha, tenha o maior número de pontos entre todos os participantes dos distribuidores autorizados ou CMP industriais;</p>
                                <p style="padding-left: 5%;">1.4.4 O gerente Destaque de toda a distribuição automotiva será aquele que, ao final da campanha, tenha o maior número de pontos, entre todos os participantes da campanha dos distribuidores automotivos.</p>
                            </div>
                            <h3><span class="number">2.0</span><span class="text"> Critérios de Pontuação</span><a href="#regulamento-10-11" class="toggle pull-right" role="button" data-toggle="collapse" aria-controls="regulamento-10-11"><i class="fa fa-minus-square-o"></i></a></h3>
                            <div id="regulamento-10-11" class="collapse in" aria-expanded="true">
                                <p>2.1 Distribuidor autorizado ou CMP industrial:</p>
                                <p style="padding-left: 5%;">2.1.1 Cada vendedor terá uma meta mensal de vendas de produtos SKF. Esta meta será definida individualmente pela direção de cada distribuidor.</p>
                                <p style="padding-left: 5%;">2.1.2 Os pontos de cada vendedor serão calculados com base no percentual atingido da meta, na proporção de 1% = 1 ponto.</p>
                                <p style="padding-left: 5%;">2.1.3 Exemplo:</p>
                                <table cellspacing="0" cellpadding="0" border="1" class="table" style="padding-left: 5%; margin-left: 5%; width: 90%;">
                                    <col width="64" />
                                    <col width="71" span="2" />
                                    <col width="45" />
                                    <col width="64" />
                                    <tr>
                                        <td width="64">Vendedor</td>
                                        <td width="71">Meta (R$)</td>
                                        <td width="71">Realizado (R$)</td>
                                        <td width="45">% da meta</td>
                                        <td width="64">Pontos</td>
                                    </tr>
                                    <tr>
                                        <td width="64">A</td>
                                        <td width="71">1.000,00</td>
                                        <td width="71">800</td>
                                        <td width="45">80%</td>
                                        <td width="64">80</td>
                                    </tr>
                                    <tr>
                                        <td width="64">B</td>
                                        <td width="71">2.000,00</td>
                                        <td width="71">2.500,00</td>
                                        <td width="45">125%</td>
                                        <td width="64">125</td>
                                    </tr>
                                    <tr>
                                        <td width="64">C</td>
                                        <td width="71">500</td>
                                        <td width="71">1.000,00</td>
                                        <td width="45">200%</td>
                                        <td width="64">200</td>
                                    </tr>
                                </table>
                                <br />
                                <p>2.2 Será disponibilizado, ao longo dos meses testes de conhecimento (quizzes) que valerão pontos durante o período da Campanha.</p>
                                <p style="padding-left: 5%;">2.2.1 Serão feitos 4 quizzes por mês, cada um com três perguntas. A cada pergunta respondida corretamente, o participante ganha um ponto.</p>
                                <p style="padding-left: 5%;">2.2.2 Duas vezes por mês será lançado um quiz especial, cada um com quatro perguntas, e que podem gerar até 8 pontos para cada participante.</p>
                                <p style="padding-left: 5%;">2.2.3 O conteúdo dos quizzes é baseado nos cursos DD College, que estão disponíveis do DD Community da SKF.</p>
                                <br />
                                <p>2.3 O total de pontos de cada participante será a soma de pontos relacionados à meta de vendas com os pontos conquistados nos quizzes.</p>
                                <br />
                                <p>2.4 Nota de Corte</p>
                                <p style="padding-left: 5%;">2.4.1 Com base nas metas de vendas definidas, será calculada uma média aritmética simples, das metas de todos os vendedores. Este resultado será a Nota de Corte;</p>
                                <p style="padding-left: 5%;">2.4.2 Só estão aptos a ganhar os prêmios intermediários, e os prêmios finais, os vendedores que realizarem vendas no valor igual ou superior à nota de corte.</p>
                                <p style="padding-left: 5%;">2.4.3 Exemplo</p>
                                <p style="padding-left: 10%;">Os vendedores A e C não estão aptos a participar da premiação mensal. Neste caso, o Vendedor E é vencedor do mês.</p>
                                <table cellspacing="0" cellpadding="0" border="1" class="table" style="padding-left: 5%; margin-left: 5%; width: 90%;">
                                    <tbody>
                                        <tr>
                                            <td width="192" colspan="2">
                                                <p align="center">Nota de corte Distribuidor Alfa</p>
                                            </td>
                                            <td width="280" colspan="3">
                                                <p align="center">R$ 300.000,00</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="72">
                                                <p align="center">Vendedor</p>
                                            </td>
                                            <td width="120">
                                                <p align="center">Meta (R$)</p>
                                            </td>
                                            <td width="110">
                                                <p align="center">Realizado (R$)</p>
                                            </td>
                                            <td width="80">
                                                <p align="center">Apto ou não</p>
                                            </td>
                                            <td width="89">
                                                <p align="center">Pontos</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="72" valign="top">
                                                <p>A</p>
                                            </td>
                                            <td width="120" valign="top">
                                                <p align="right">100.000</p>
                                            </td>
                                            <td width="110" valign="top">
                                                <p align="right">150.000</p>
                                            </td>
                                            <td width="80" valign="top">
                                                <p align="center">Não</p>
                                            </td>
                                            <td width="89" valign="top">
                                                <p align="center">150</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="72" valign="top">
                                                <p>B</p>
                                            </td>
                                            <td width="120" valign="top">
                                                <p align="right">200.000</p>
                                            </td>
                                            <td width="110" valign="top">
                                                <p align="right">380.000</p>
                                            </td>
                                            <td width="80" valign="top">
                                                <p align="center">Sim</p>
                                            </td>
                                            <td width="89" valign="top">
                                                <p align="center">190</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="72" valign="top">
                                                <p>C</p>
                                            </td>
                                            <td width="120" valign="top">
                                                <p align="right">500.000</p>
                                            </td>
                                            <td width="110" valign="top">
                                                <p align="right">340.000</p>
                                            </td>
                                            <td width="80" valign="top">
                                                <p align="center">Não</p>
                                            </td>
                                            <td width="89" valign="top">
                                                <p align="center">68</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="72" valign="top">
                                                <p>D</p>
                                            </td>
                                            <td width="120" valign="top">
                                                <p align="right">600.000</p>
                                            </td>
                                            <td width="110" valign="top">
                                                <p align="right">650.000</p>
                                            </td>
                                            <td width="80" valign="top">
                                                <p align="center">Sim</p>
                                            </td>
                                            <td width="89" valign="top">
                                                <p align="center">108</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="72" valign="top">
                                                <p>E</p>
                                            </td>
                                            <td width="120" valign="top">
                                                <p align="right">100.000</p>
                                            </td>
                                            <td width="110" valign="top">
                                                <p align="right">305.000</p>
                                            </td>
                                            <td width="80" valign="top">
                                                <p align="center">Sim</p>
                                            </td>
                                            <td width="89" valign="top">
                                                <p align="center">305</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p style="padding-left: 5%;">2.4.4 A mecânica se repete para o resultado final. Será calculada uma nota de corte, com base nas metas de vendas de todo o período. Só serão indicados os três (3)  vendedores com o maior número de pontos, e que tiverem realizado vendas de produtos SKF,  iguais ou superiores à nota de corte da Distribuidora.</p>
                                <p style="padding-left: 5%;">2.4.5 O vencedor do prêmio Destaque de Vendas SKF 2016 será o vendedor com o maior número de pontos entre os vendedores de todos os distribuidores Industriais que tiverem sido indicados para participar do evento, e que tenha feito vendas de produtos SKF superiores ou iguais à nota de corte que será calculada considerando todas as metas de vendas de produtos SKF de todos os vendedores participantes da campanha.</p>
                                <p style="padding-left: 5%;">2.4.6 Em caso de empate no número de pontos, será escolhido vencedor o vendedor que:</p>
                                <p style="padding-left: 10%;">2.4.6.1 Tiver o maior valor em R$ vendidos em produtos SKF no período da campanha;</p>
                                <p style="padding-left: 10%;">2.4.6.2 Acertado o maior número de perguntas dos quizzes;</p>
                                <p style="padding-left: 10%;">2.4.6.3 Dia e hora de resposta dos quizzes; aquele que responder mais cedo, será o vencedor.</p>
                            </div>
                            <h3><span class="number">3.0</span><span class="text"> Distribuidor automotivo</span><a href="#regulamento-10-12" class="toggle pull-right" role="button" data-toggle="collapse" aria-controls="regulamento-10-12"><i class="fa fa-minus-square-o"></i></a></h3>
                            <div id="regulamento-10-12" class="collapse in" aria-expanded="true">
                                <p>3.1 Cada gerente de filial terá uma meta mensal de vendas de produtos SKF. Esta meta será definida individualmente pela direção de cada distribuidor em conjunto com a equipe de vendas da SKF.</p>
                                <p>3.2 Os pontos de cada gerente de filial serão calculados com base no percentual atingido da meta, na proporção de 1% = 1 ponto.</p>
                                <p>3.3 Exemplo:</p>
                                <table cellspacing="0" cellpadding="0" border="1" class="table" style="padding-left: 5%; margin-left: 5%; width: 90%;">
                                    <tr>
                                        <td width="110" valign="top">
                                            <p align="center">Gerente</p>
                                        </td>
                                        <td width="88" valign="top">
                                            <p>Meta (R$)</p>
                                        </td>
                                        <td width="108" valign="top">
                                            <p>Realizado (R$)</p>
                                        </td>
                                        <td width="89" valign="top">
                                            <p>% da meta</p>
                                        </td>
                                        <td width="77" valign="top">
                                            <p>Pontos</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="110" valign="top">
                                            <p align="center">Filial A</p>
                                        </td>
                                        <td width="88" valign="top">
                                            <p align="right">1.000,00</p>
                                        </td>
                                        <td width="108" valign="top">
                                            <p align="right">800,00</p>
                                        </td>
                                        <td width="89" valign="top">
                                            <p align="center">80 %</p>
                                        </td>
                                        <td width="77" valign="top">
                                            <p align="center">80</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="110" valign="top">
                                            <p align="center">Filial B</p>
                                        </td>
                                        <td width="88" valign="top">
                                            <p align="right">2.000,00</p>
                                        </td>
                                        <td width="108" valign="top">
                                            <p align="right">2.500,00</p>
                                        </td>
                                        <td width="89" valign="top">
                                            <p align="center">125 %</p>
                                        </td>
                                        <td width="77" valign="top">
                                            <p align="center">125</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="110" valign="top">
                                            <p align="center">Filial C</p>
                                        </td>
                                        <td width="88" valign="top">
                                            <p align="right">500,00</p>
                                        </td>
                                        <td width="108" valign="top">
                                            <p align="right">1.000,00</p>
                                        </td>
                                        <td width="89" valign="top">
                                            <p align="center">200 %</p>
                                        </td>
                                        <td width="77" valign="top">
                                            <p align="center">200</p>
                                        </td>
                                    </tr>
                                    <br />
                                </table>
                                <p>3.4 Ao longo da campanha, mensalmente serão definidos produtos que possibilitarão aos gerentes a obtenção de pontos extras. São os pontos coringas, definidos no início de cada mês. Estes produtos terão meta de vendas definidos pela SKF, e o percentual do atingimento destas metas específicas, serão transformadas em pontos, que serão somados ao resultado de cada gerente.</p>
                                <p>3.5 Ao final da campanha serão indicados os três (3) gerentes de filial com o maior número de pontos. O que tiver o maior número de pontos ganhará o prêmio final do distribuidor. Será entregue um prêmio para cada distribuidor participante.</p>
                                <p>3.6 O vencedor do prêmio Destaque de Vendas SKF 2016 será o gerente de filial com o maior número de pontos entre os gerentes de todos os distribuidores automotivos que tiverem sido indicados para participar do evento.</p>
                                <p>3.7 Em caso de empate no número de pontos, será escolhido vencedor o gerente que:</p>
                                <p style="padding-left: 5%;">3.7.1 Tiver o maior valor em R$ vendidos em produtos SKF no período da campanha;</p>
                                <p style="padding-left: 5%;">3.7.2 Obtido o maior número de pontos coringas</p>
                            </div>
                            <h3><span class="number">4.0</span><span class="text"> Sobre os prêmios</span><a href="#regulamento-10-13" class="toggle pull-right" role="button" data-toggle="collapse" aria-controls="regulamento-10-13"><i class="fa fa-minus-square-o"></i></a></h3>
                            <div id="regulamento-10-13" class="collapse in" aria-expanded="true">
                                <p>4.1 Prêmios intermediários – mensalmente será premiado com um vale compras no valor de R$ 100,00  um (1) vendedor de cada distribuidor autorizado ou CMP industrial e um (1) gerente de filial dos distribuidores autorizados automotivos que tiverem alcançado o maior número de pontos.</p>
                                <p>4.2 Evento SKF – ao fim da campanha, os três vendedores ou gerentes, melhor colocados em cada distribuidor autorizado ou CMP Industrial e distribuidor autorizado automotivo serão convidados a participar do evento de premiação Seleção SKF, no estado de São Paulo, em local a ser definido, no mês de dezembro de 2016, quando será anunciado o vencedor final de cada distribuidor autorizado, e também o vendedor e o gerente Destaque SKF 2016. A SKF arcará com os custos de transporte dos três (3) indicados de cada distribuidor.</p>
                                <p style="padding-left: 5%;">4.2.1 O vendedor ou gerente que tiver ganho o maior número de pontos durante a campanha, em cada um dos distribuidores participantes ganhará um vale compras no valor de R$ 3.000,00, que poderá ser utilizado para aquisição de produtos e serviços, a escolha do vencedor. Será distribuído um prêmio de R$ 3.000,00 para cada distribuidor participante.</p>
                                <p style="padding-left: 5%;">4.2.2 Os vencedores do prêmio Destaque de Vendas SKF 2016 ganhará um Cruzeiro pelo litoral brasileiro, com direito a levar três (3) pessoas como acompanhantes.</p>
                            </div>
                            <h3><span class="number">5.0</span><span class="text"> As responsabilidades</span><a href="#regulamento-10-14" class="toggle pull-right" role="button" data-toggle="collapse" aria-controls="regulamento-10-14"><i class="fa fa-minus-square-o"></i></a></h3>
                            <div id="regulamento-10-14" class="collapse in" aria-expanded="true">
                                <br />
                                <p>Distribuidor industrial e automotivo</p>
                                <br />
                                <p style="padding-left: 5%;">1. Definir as metas mensais de vendas de produtos SKF para cada um dos vendedores e gerentes participantes;</p>
                                <p style="padding-left: 5%;">2. Informar para os responsáveis pela campanha as metas individuais de cada vendedor pelo e-mail <a href="mailto:contato@selecaoskf.com.br">contato@selecaoskf.com.br</a></p>
                                <p style="padding-left: 5%;">3. Informar mensalmente os resultados de vendas de produtos SKF, de cada vendedor ou gerente participante da campanha, até o terceiro dia útil de cada mês, posterior ao mês apurado, via e-mail para <a href="mailto:contato@selecaoskf.com.br">contato@selecaoskf.com.br</a></p>
                                <p style="padding-left: 5%;">4. Acompanhar os resultados públicos no site da campanha e alertar a organização, caso haja alguma discrepância.</p>
                                <br />
                                <p>SKF</p>
                                <br />
                                <p style="padding-left: 5%;">1. Criar e manter sistema de acompanhamento, controle e informação sobre o andamento da campanha.</p>
                                <p style="padding-left: 5%;">2. Providenciar os prêmios intermediários e prêmios finais para os participantes que cumprirem as condições deste regulamento.</p>
                                <p style="padding-left: 5%;">3. Divulgar os vencedores mensais até o quinto dia útil de cada mês, e posterior entrega dos prêmios.</p>
                                <p style="padding-left: 5%;">4. Aquisição de todos os prêmios da campanha.</p>
                                <p style="padding-left: 5%;">5. Providenciar transporte para os participantes do evento Seleção SKF, que acontecerá em dezembro de 2016.</p>
                                <p style="padding-left: 5%;">6. Não divulgar entre a rede os valores apurados por cada distribuidor.</p>
                            </div>
                            <h3><span class="number">6.0</span><span class="text"> Outras informações</span><a href="#regulamento-10-15" class="toggle pull-right" role="button" data-toggle="collapse" aria-controls="regulamento-10-15"><i class="fa fa-minus-square-o"></i></a></h3>
                            <div id="regulamento-10-15" class="collapse in" aria-expanded="true">
                                <p>6.1 A divulgação dos resultados dessa campanha será realizada através do site da campanha em dezembro de 2016.</p>
                                <p>6.2 Serão desclassificados os vendedores e gerentes que agirem em desacordo com as regras estabelecidas neste regulamento, inclusive através de atos fraudulentos, sendo de plena responsabilidade deste, todo e qualquer dano causado pela eventual prática de atos ilícitos.</p>
                                <p>6.3 Só estarão aptos a ganhar os prêmios os vendedores ou gerentes que tiverem seu nome e resultados publicados no site, no dia 5 de dezembro de 2016. E da responsabilidade de cada participante, acompanhar e verificar seus resultados no site <a href="www.selecaoskf.com.br.">www.selecaoskf.com.br.</a></p>
                                <p>6.4 Apenas serão elegíveis a presente campanha os vendedores e gerente que estiverem efetivos e ativos em todo o período compreendido por esta campanha, não podendo estar em cumprimento de aviso prévio, tampouco ser funcionário temporário.</p>
                                <p>6.5 Não será atribuída a premiação no caso de desligamento (voluntário e/ou forçado) do vendedor ou gerente do quadro de empregados dos distribuidores autorizados industriais ou automotivos, mesmo que este já esteja cadastrado na Campanha.</p>
                                <p>6.6 Os participantes da campanha poderão enviar suas dúvidas e sugestões através do site campanha, via e-mail <a href="mailto:contato@selecaoskf.com.br">contato@selecaoskf.com.br</a>  ou por meio de Whatsapp (11) 9.7470.2952</p>
                                <p>6.7 Os casos omissos, dúvidas ou divergências serão julgados por uma comissão formada por funcionários da SKF do Brasil cujas decisões serão soberanas e irrecorríveis.</p>
                                <p>6.8 Os participantes da presente campanha autorizam, desde já, o uso de sua imagem, som de voz e nome na internet, em filmes, vídeos, fotos e cartazes, anúncios em jornais, revistas e televisão, toda mídia impressa ou eletrônica, pelo prazo de 01 (um) ano, a contar da data do término desta, para eventuais comunicações desta Campanha de Incentivo, sem qualquer ônus para a SKF do Brasil</p>
                                <p>6.9 A participação desta campanha implica no conhecimento e aceitação total e irrestrita de todos os termos e condições deste Regulamento.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
