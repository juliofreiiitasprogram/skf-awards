﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SKFEntity;
using SKFBfl;
using System.Web.UI.HtmlControls;

namespace WebSite
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userEntity = new UserEntity();

            if (Session["UserName"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
               
                lblNome.Text = Convert.ToString(Session["UserName"]);
                montaNoticias();
            }
        }

        public void montaNoticias()
        {
            var newsEntity = new NewEntity();
            var newsBfl = new NewBfl();

            List<NewEntity> listarNews = newsBfl.ListarNoticias(newsEntity);

            for (int i = 0; i < listarNews.Count; i++)
            {
                NewEntity news = listarNews[i];

                HtmlGenericControl divNoticia = new HtmlGenericControl();
                divNoticia.InnerHtml =
                "<div class=\"item row\">" +
                                            "<div class=\"col-xs-12 col-sm-4\">" +
                                                "<div class=\"image\">" +
                                                    "<a href=\"javascript:abrirNoticia" + i + "();\"><img src=\"assets/news/"+ news.imagem +"\" alt=\"\" width=\"270\" height=\"140\"></a>" +
                                                "</div>" +
                                                //"<div class=\"info\">" +
                                                //    "<a href=\"javascript:abrirNoticia" + i + "();\" class=\"btn btn-link\">" +
                                                //       " <div class=\"comments info-item\">" +
                                                //            "<div class=\"icon\">" +
                                                //                "<i class=\"fa fa-comments-o\"></i>" +
                                                //            "</div>" +
                                                //            "<div class=\"text\">" +
                                                //                "12" +
                                                //            "</div>" +
                                                //        "</div>" +
                                                //    "</a>" +
                                                //"</div>" +
                                            "</div>" +
                                            "<div class=\"col-xs-12 col-sm-8\">" +
                                                "<div class=\"head\">" +
                                                    "<div class=\"meta\">" +
                                                        "<div class=\"meta-item\">" +
                                                            "<div class=\"icon\">" +
                                                                "<i class=\"fa fa-clock-o\"></i>" +
                                                            "</div>" +
                                                            "<div class=\"text\">"
                                                                + news.Hora +
                                                            "</div>" +
                                                        "</div>" +
                                                        "<div class=\"meta-item\">" +
                                                            "<div class=\"icon\">" +
                                                                "<i class=\"fa fa-calendar-o\"></i>" +
                                                            "</div>" +
                                                            "<div class=\"text\">" +
                                                                news.Data +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                    "<div class=\"title\">" +
                                                        "<a href=\"javascript:abrirNoticia" + i + "();\">" + news.Titulo + "</a>" +
                                                    "</div>" +
                                                "</div>" +
                                                "<div class=\"body\">" +
                                                    "<div class=\"content\">" +
                                                        "<div class=\"desc\">" +
                                                            news.SubTitulo +
                                                        "</div>" +
                                                    "</div>" +
                                                "</div>" +
                                            "</div>" +
                                        "</div>";
                PlaceHolder1.Controls.Add(divNoticia);

                HtmlGenericControl openModal = new HtmlGenericControl();
                openModal.InnerHtml = "<script>function abrirNoticia" + i + "() {" +
                    "$(\"#modalNews" + i + "\").modal('show');}</script>";

                PlaceHolder2.Controls.Add(openModal);

                HtmlGenericControl modalContent = new HtmlGenericControl();
                modalContent.InnerHtml = "<div id=\"modalNews" + i + "\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\">" +
                                    "<div class=\"modal-dialog modal-lg\" role=\"document\">" +
                                        "<div class=\"modal-content\">" +
                                            "<div class=\"modal-header\">" +
                                                "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                                                "<h4 class=\"modal-title\" id=\"myModalLabel\">Modal title</h4>" +
                                               "<div style=\"color:#000;\">"+
                                                    "<strong>" + news.Titulo + "</strong>"+
                                                "</div>" +
                                            "</div>" +
                                            "<div class=\"modal-body\">"+
                                            "<div style=\"color:#000;\">" +
                                                news.Texto +
                                                "</div>" +
                                              "</div>"+
                                        "</div>" +
                                    "</div>" +
                                "</div>";
                PlaceHolder3.Controls.Add(modalContent);


            }

        }
    }
}