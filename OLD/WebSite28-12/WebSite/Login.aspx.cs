﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SKFEntity;
using SKFBfl;

namespace WebSite
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userEntity = new UserEntity();

            if (Session["UserName"] != null)
            {
                Response.Redirect("Default.aspx");
            }
        }

        protected void btnEntrar_Click(object sender, EventArgs e)
        {
            // Criar uma instancia com do objeto User
            var userEntity = new UserEntity();
            var userBfl = new UserBfl();

            userEntity.UserMail = txtUser.Text;
            userEntity.UserPassword = txtPassword.Text;

            // Efetuar o Login passando os parametros
            userBfl.PesquisarUsuario(userEntity);

            if (userEntity.UserMail != "" && userEntity.UserPassword != "")
            {
                    Session["UserId"] = userEntity.UserId;
                    Session["UserName"] = userEntity.UserName;
                    Session["UserPerfil"] = userEntity.UserPerfil;
                    Session["UserDistribuidora"] = userEntity.UserDistribuidora;

                    Response.Redirect("Default.aspx", false);

            }
            else
            {
                user.Visible = true;
            }
        }

    }
}