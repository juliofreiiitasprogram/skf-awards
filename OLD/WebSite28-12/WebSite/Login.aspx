﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebSite.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Seleção SKF</title>

    <link rel="apple-touch-icon" sizes="57x57" href="assets/fav/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/fav/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/fav/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/fav/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/fav/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/fav/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/fav/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/fav/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/fav/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192"  href="assets/fav/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/fav/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/fav/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png" />
    <link rel="manifest" href="assets/fav/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="assets/fav/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/css/style.css" />
    <script src="https://use.fontawesome.com/b30d948250.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
        <div id="site">
            <div class="bg-login"></div>
            <div id="login">
                <div class="modal fade" id="loginform" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="">Login</h4>
                                <div class="modal-subtitle">
                                    <em>Preencha as informações abaixo para acessar o site.</em>
                                </div>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user-circle-o"></i></span>
                                        <asp:TextBox ID="txtUser" runat="server" class="form-control" placeholder="Seu login"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                        <asp:TextBox ID="txtPassword" runat="server" type="password" class="form-control" placeholder="Sua senha"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <p class="bg-warning" style="color: #808080;" runat="server" id="user" visible="false">O e-mail e a senha que você digitou não coincidem.</p>
                            <div class="modal-footer">
                                <%--<button type="button" class="btn btn-default" ><i class="fa fa-sign-in"></i>Entrar</button>--%>
                                <asp:Button ID="btnEntrar" class="btn btn-default btn-entrar" runat="server" Text="Entrar" OnClick="btnEntrar_Click" />
                                <button type="button" onclick="javascript:divSuport();" class="btn btn-default btn-outline btn-suporte"><i class="fa fa-question-circle-o"></i>Suporte</button>

                            </div>
                            <div class="bg-success" style="color: #808080; padding: 5%" runat="server" id="P1">
                                <p><strong>Olá, você precisa de ajuda?</strong></p>

                                <p>Use alguma da nossa assistência:</p>
                                <div style="width: 100%">
                                    <a href="tel:+551156436614">
                                        <img src="assets/img/icon_cell.png" width="30%" /></a>
                                    <a href="tel:+5511982054664">
                                        <img src="assets/img/icon_whatsapp.png" width="30%" /></a>
                                    <a href="mailto:contato@selecaoskf.com.br">
                                        <img src="assets/img/icon_maill.png" width="30%" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <script>
            $("#P1").hide();
            function divSuport() {
                $("#P1").toggle();
            }
        </script>
        <script>
            $('#loginform').modal({ show: true, backdrop: 'static', keyboard: false });
        </script>
    </form>
</body>
</html>
