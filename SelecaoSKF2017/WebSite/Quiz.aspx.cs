﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SKFBfl;
using SKFEntity;
using System.Web.Services;

namespace WebSite
{
    public partial class Quiz : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var UserID = Session["UserId"];
            var CodeTipo = Session["CodeTipo"];
            var quizEntity = new QuizEntity();
            var quizBfl = new QuizBfl();
            var Day = DateTime.Now.Day;
            var Month = DateTime.Now.Month;
            var year = DateTime.Now.Year;
            var date = Day + "/" + Month + "/" + year;

            quizEntity.DataQuestao = date;
            quizEntity.UserID = Convert.ToInt32(UserID);

            quizBfl.PesquisarResposta(quizEntity);

            if (quizEntity.numberQuestion == "")
            {

                quizEntity.DataQuestao = Convert.ToString(date);
                quizEntity.TipoEmpresa = Convert.ToString(CodeTipo);

                List<QuizEntity> listQuiz = quizBfl.ListarQuestoes(quizEntity);

                if (listQuiz.Count == 0)
                {
                    lblMensagem.Text = "Hoje não tem pergunta disponível. Perguntas são todas terças e quintas-feiras.";
                }
                else
                {
                    questoesDiv.Visible = true;
                    lblMensagem.Text = "Responda rápido e acumule pontos";

                    lblNumberQuestion.Text = listQuiz[0].numberQuestion;
                    lblQuestion.Text = listQuiz[0].Questao;
                    hfQuestionID.Value = Convert.ToString(listQuiz[0].QuestaoID);
                    hfUserID.Value = Convert.ToString(UserID);

                    lblPerguntaA.Text = listQuiz[0].Alternativa;
                    lblPerguntaB.Text = listQuiz[1].Alternativa;
                    lblPerguntaC.Text = listQuiz[2].Alternativa;
                    lblPerguntaD.Text = listQuiz[3].Alternativa;

                }
            }
            else
            {
                lblMensagem.Text = "A pergunta de hoje já foi respondida por você.";
            }
        }

        [WebMethod]
        public static object GetDate(string questaoID, string radio, string userID)
        {
            var resp1 = radio;
            var QuizID = questaoID;
            var UserID = userID;

            var quizBfl = new QuizBfl();
            var quizEntity = new QuizEntity();

            quizEntity.QuestaoID = Convert.ToInt32(QuizID);
            quizEntity.Alternativa = resp1;
            quizEntity.UserID = Convert.ToInt32(UserID);

            quizBfl.RegistrarPonto(quizEntity);

            return resp1;
        }

       

    }
}