﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Quiz.aspx.cs" Inherits="WebSite.Quiz" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="quiz" class="col-xs-12 space news">
        <div class="head">
            <div class="title">Quiz</div>

            <div class="desc">
                <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label></div>
        </div>
        <div id="questoesDiv" runat="server" visible="false">
            <div class="content">
                <div class="quiz-list">
                    <div class="question">
                        <div class="head">
                            <div class="number">
                                <asp:Label ID="lblNumberQuestion" value="" runat="server" Text=""></asp:Label>
                                <asp:HiddenField ID="hfQuestionID" Value="" runat="server" />
                                <asp:HiddenField ID="hfUserID" Value="" runat="server" />
                            </div>
                            <div class="title">
                                <asp:Label ID="lblQuestion" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="answer-list">

                        <div class="radio answer-list-item">
                            <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="A">
                                <span class="radio-indicator">A</span>
                                <span class="radio-label">
                                    <asp:Label ID="lblPerguntaA" runat="server" Text=""></asp:Label></span>
                            </label>
                        </div>

                        <div class="radio answer-list-item">
                            <label>
                                <input type="radio" name="optionsRadios" id="Radio1" value="B">
                                <span class="radio-indicator">B</span>
                                <span class="radio-label">
                                    <asp:Label ID="lblPerguntaB" runat="server" Text=""></asp:Label></span>
                            </label>
                        </div>

                        <div class="radio answer-list-item">
                            <label>
                                <input type="radio" name="optionsRadios" id="Radio2" value="C">
                                <span class="radio-indicator">C</span>
                                <span class="radio-label">
                                    <asp:Label ID="lblPerguntaC" runat="server" Text=""></asp:Label></span>
                            </label>
                        </div>

                        <div class="radio answer-list-item">
                            <label>
                                <input type="radio" name="optionsRadios" id="Radio3" value="D">
                                <span class="radio-indicator">D</span>
                                <span class="radio-label">
                                    <asp:Label ID="lblPerguntaD" runat="server" Text=""></asp:Label></span>
                            </label>
                        </div>

                        <div class="btn-group">
                            <button id="Enviar" type="button" class="btn btn-primary btn-outline">
                                Enviar resposta
                            </button>
                            <%--<asp:Button ID="btnEnviar" runat="server" class="btn btn-primary btn-outline" OnClick="btnEnviar_Click" Text="Enviar resposta" />--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        //$("form").on("submit", function (event) {
        //    event.preventDefault();
        //    $(this).serialize();
        //});
        document.getElementById("Enviar").onclick = function () {
            var radio1 = document.getElementsByName("optionsRadios");
            var idquestion = document.getElementById("ContentPlaceHolder1_hfQuestionID");
            var userid = document.getElementById("ContentPlaceHolder1_hfUserID");
            var divQuestao = document.getElementById('ContentPlaceHolder1_questoesDiv');

            var questao = new Object();
            idquestao = idquestion.value;
            iduser = userid.value;

            console.log(idquestao);
            console.log(iduser);

            for (var i = 0; i < radio1.length; i++) {
                if (radio1[i].checked) {
                    console.log("Escolheu: " + radio1[i].value);
                    radio1 = radio1[i].value;
                    
                }
            }
                $.ajax({
                    type: "POST",
                    url: "Quiz.aspx/GetDate",
                    data: "{radio:'" + radio1 + "', questaoID:'" + idquestao + "', userID:'" + iduser + "'}",
                    contentType: "application/json; charset=utf-8",
                    dateType: "json",
                    success:
                        function (msg) {
                            console.log("Parabens");
                            $(divQuestao).html('<div class="text-success"><i class="fa fa-check"></i>Resposta gravada com sucesso, aguarde o próximo quiz.</div>');
                        }


                });

        };
        </script>
</asp:Content>
