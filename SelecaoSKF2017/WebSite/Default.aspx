﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebSite.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Seleção SKF</title>

    <link rel="apple-touch-icon" sizes="57x57" href="assets/fav/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/fav/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/fav/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/fav/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/fav/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/fav/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/fav/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/fav/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/fav/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="assets/fav/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/fav/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/fav/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png" />
    <link rel="manifest" href="assets/fav/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="assets/fav/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/css/style.css" />
    <script src="https://use.fontawesome.com/b30d948250.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        #btnSair {
            text-align:left;
        }

       
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:PlaceHolder ID="PlaceHolder3" runat="server"></asp:PlaceHolder>
        <%--<div id="modalNews" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                        
                    </div>
                </div>
            </div>
        </div>--%>
        <div id="site">
            <header id="header">
                <div class="component">

                    <div id="slider">
                        <div class="context">
                            <div class="logo">
                                <img src="assets/img/logo.png" alt="" width="142" height="215">
                            </div>
                            <div class="text">
                                <div class="middle">
                                    <div class="box">
                                        <div class="content">
                                            <div class="title"></div>
                                            <div class="subtitle">A torcida da seleção skf só aumenta!</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="image">
                            <div class="bg-1"></div>
                            <div class="bg-2"></div>
                            <div class="bg-3"></div>
                        </div>
                    </div>
                    <div id="menu">
                        <nav class="navbar navbar-default" role="navigation">
                            <div class="container">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="#"></a>
                                </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse" id="navbar">
                                    <ul class="nav navbar-nav">
                                        <li class="active"><a href="Default.aspx">Home</a></li>
                                        <%--<li><a href="quiz.html">Quiz</a></li>--%>
                                        <li><a href="Regulamento.aspx">Regulamento</a></li>
                                        <li><a href="Contato.aspx">Contato</a></li>

                                    </ul>
                                </div>
                                <!-- /.navbar-collapse -->
                            </div>
                            <!-- /.container-fluid -->
                        </nav>
                    </div>
                    <div id="logo-line">
                        <div class="container-fluid">
                            <div class="line row">
                                <div class="pull-right">
                                    <div class="logo"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div id="page">
                <div class="component">
                    <div class="container">
                        <div class="row">
                            <div id="news" class="col-xs-12 col-md-8 space news">
                                <div class="head">
                                    <div class="title">Notícias</div>
                                    <%-- <div class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</div>--%>
                                </div>
                                <div class="content">
                                    <div class="items">

                                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>

                                    </div>
                                </div>
                            </div>
                            <div id="minha-conta" class="col-xs-12 col-md-4 space account">
                                <div class="head">
                                    <div class="title">Minha conta</div>
                                    <div class="desc">Informações sobre a sua conta.</div>
                                </div>
                                <div class="content">
                                    <div class="name">
                                        Olá, <strong>
                                            <asp:Label ID="lblNome" runat="server" Text=""></asp:Label></strong>
                                    </div>
                                    <div class="links">
                                        <ul class="list-inline">
                                            <%--<li><a href="#" class="btn btn-link"><i class="fa fa-edit"></i> Editar</a></li>--%>
                                            <button runat="server" id="btnSair" class="btn btn-link" onserverclick="btnSair_ServerClick"><i class="fa fa-power-off"></i> Sair</button>
                                        </ul>
                                    </div>
                                    <%--<div class="quizes">
                                    <div class="head">
                                        <div class="title">Quiz Ativo</div>
                                    </div>
                                    <div class="body">
                                        <div class="number">02</div>
                                        <div class="context">
                                            <div class="question">Officia laborum eiusmod eiusmod commodo et nisi occaecat aliquip adipisicing?</div>
                                            <div class="answer checked"><a href="#"><i class="fa fa-check"></i> Respondido</a></div>
                                            <div class="answer not-checked"><a href="#"><i class="fa fa-circle-o-notch fa-spin"></i> Responder ao Quiz</a></div>
                                        </div>
                                    </div>
                                </div>--%>
                                </div>
                            </div>
                            <div id="ranking" class="col-xs-12 col-md-4 space rank">
                                <div class="head">
                                    <div class="title">Ranking</div>
                                    <div class="desc">O vencedor será aquele que acumular o maior número de ponto ao fim da campanha. Consulte seu gerente para conhecer sua meta de vendas.</div>
                                </div>
                                <div class="content">
                                    <div class="select row">
                                        <div class="mes select-item col-xs-12 col-sm-6 text-center">
                                            <div class="heading">
                                                Mês
                                            </div>
                                            <div class="selecting">
                                                <select class="" name="ranking-mes">
                                                    <option value="">Selecione</option>
                                                    <option value="">Janeiro</option>
                                                    <option value="">Fevereiro</option>
                                                    <option value="">Março</option>
                                                    <option value="">Abril</option>
                                                    <option value="">Maio</option>
                                                </select>
                                            </div>
                                        </div>
                                        <%--<div class="distribuidora select-item col-xs-12 col-sm-6 text-center">
                                            <div class="heading">
                                                Distribuidora
                                            </div>
                                            <div class="selecting">
                                                <select class="" name="ranking-mes">
                                                    <option value="">Selecione</option>
                                                </select>
                                            </div>
                                        </div>--%>
                                    </div>
                                    <div class="ranking row">
                                        <div class="ranking-item col-xs-12 text-center">
                                            <div class="heading">
                                                Ranking
                                            </div>
                                            <div class="rank-table">
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            Sem dados no momento.
                                                        </tr>
                                                        <%-- <tr>
                                                            <td>
                                                                <div class="number">001</div>
                                                            </td>
                                                            <td>
                                                                <div class="name"><a href="#">Dados não informado.</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="percent"><i class="fa fa-pie-chart"></i>001%</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="number">001</div>
                                                            </td>
                                                            <td>
                                                                <div class="name"><a href="#">Miriam Marchioli</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="percent"><i class="fa fa-pie-chart"></i>001%</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="number">002</div>
                                                            </td>
                                                            <td>
                                                                <div class="name"><a href="#">John Doe</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="percent"><i class="fa fa-pie-chart"></i>002%</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="number">003</div>
                                                            </td>
                                                            <td>
                                                                <div class="name"><a href="#">André Matos</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="percent"><i class="fa fa-pie-chart"></i>003%</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="number">004</div>
                                                            </td>
                                                            <td>
                                                                <div class="name"><a href="#">Cintia Elisa</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="percent"><i class="fa fa-pie-chart"></i>004%</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="number">005</div>
                                                            </td>
                                                            <td>
                                                                <div class="name"><a href="#">Marcos Salazar</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="percent"><i class="fa fa-pie-chart"></i>005%</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="number">006</div>
                                                            </td>
                                                            <td>
                                                                <div class="name"><a href="#">Ruben Lucas</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="percent"><i class="fa fa-pie-chart"></i>006%</div>
                                                            </td>
                                                        </tr>--%>
                                                    </tbody>
                                                </table>
                                                <%--<div class="more">
                                                    <a href="#ranking-expand" class="btn btn-link" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="ranking-expand"><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i></a>
                                                    <div id="ranking-expand" class="expand-more collapse">
                                                        <table class="table">

                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="number">007</div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="name"><a href="#">Miriam Marchioli</a></div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="percent"><i class="fa fa-pie-chart"></i>007%</div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="number">008</div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="name"><a href="#">John Doe</a></div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="percent"><i class="fa fa-pie-chart"></i>008%</div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="number">009</div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="name"><a href="#">André Matos</a></div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="percent"><i class="fa fa-pie-chart"></i>009%</div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="number">010</div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="name"><a href="#">Cintia Elisa</a></div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="percent"><i class="fa fa-pie-chart"></i>010%</div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="number">011</div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="name"><a href="#">Marcos Salazar</a></div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="percent"><i class="fa fa-pie-chart"></i>011%</div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="number">012</div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="name"><a href="#">Ruben Lucas</a></div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="percent"><i class="fa fa-pie-chart"></i>012%</div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer id="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="line"></div>
                        </div>
                        <div class="col-xs-12 text-center space menu-footer">
                            <ul class="list-inline">
                                <li><a href="Default.aspx">Home</a></li>
                                <%-- <li><a href="quiz.html">Quiz</a></li>--%>
                                <li><a href="Regulamento.aspx">Regulamento</a></li>
                                <li><a href="Contato.aspx">Contato</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.interactive_bg.js"></script>
        <script>
            $('#slider').mousemove(function (e) {
                // This condition prevents transition from causing the movement of the background to lag
                var defaults = {
                    strength: 12,
                    scale: 1,
                    animationSpeed: "100000ms",
                    contain: true,
                    wrapContent: false
                };

                var settings = $.extend({}, defaults),
                    el = $(this),
                    h = el.outerHeight(),
                    w = el.outerWidth(),
                    sh = settings.strength / h,
                    sw = settings.strength / w,
                    has_touch = 'ontouchstart' in document.documentElement;

                var pageX = e.pageX || e.clientX,
                pageY = e.pageY || e.clientY,
                pageX = (pageX - $(this).offset().left) - (w / 2),
                pageY = (pageY - $(this).offset().top) - (h / 2),
                newX = ((sw * pageX)) * -1,
                newY = ((sh * pageY)) * -1;
                // Use matrix to move the background from its origin
                // Also, disable transition to prevent lag
                $('#slider .context .text').css({
                    "-webkit-transform": "matrix(" + settings.scale + ",0,0," + settings.scale + "," + newX * 7 + "," + newY * 1.2 + ")",
                    "-moz-transform": "matrix(" + settings.scale + ",0,0," + settings.scale + "," + newX * 7 + "," + newY * 1.2 + ")",
                    "-o-transform": "matrix(" + settings.scale + ",0,0," + settings.scale + "," + newX * 7 + "," + newY * 1.2 + ")",
                    "transform": "matrix(" + settings.scale + ",0,0," + settings.scale + "," + newX * 7 + "," + newY * 1.2 + ")",
                    "-webkit-transition": "none",
                    "-moz-transition": "none",
                    "-o-transition": "none",
                    "transition": "none"
                });
                $('#slider .bg-2').css({
                    "-webkit-transform": "matrix(" + settings.scale + ",0,0," + settings.scale + "," + newX * 3 + "," + newY + ")",
                    "-moz-transform": "matrix(" + settings.scale + ",0,0," + settings.scale + "," + newX * 3 + "," + newY + ")",
                    "-o-transform": "matrix(" + settings.scale + ",0,0," + settings.scale + "," + newX * 3 + "," + newY + ")",
                    "transform": "matrix(" + settings.scale + ",0,0," + settings.scale + "," + newX * 3 + "," + newY + ")",
                    "-webkit-transition": "none",
                    "-moz-transition": "none",
                    "-o-transition": "none",
                    "transition": "none"
                });
            });
        </script>
        <asp:PlaceHolder ID="PlaceHolder2" runat="server"></asp:PlaceHolder>
        <script>
            $("#login-form").modal('show');
            //function abrirNoticia() {
            //    $("#modalNews").modal('show');
            //}
        </script>
    </form>
</body>
</html>
