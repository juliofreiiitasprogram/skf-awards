﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Regulamento.aspx.cs" Inherits="WebSite.Regulamento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div id="regulamento" class="col-xs-12 space simple-page">
            <div class="head">
                <div class="title">Regulamento</div>
            </div>
            <div class="content">
                <div class="body">
                    <div class="post-content regulamento-items">
                        <div class="regulamento-item">
                            <%--<h2><span class="number">1.0</span><span class="text"> A campanha</span></h2>--%>
                            <h3><span class="number">1.0</span><span class="text"> A campanha</span><a href="#regulamento-10-10" class="toggle pull-right" role="button" data-toggle="collapse" aria-controls="regulamento-10-10"><i class="fa fa-minus-square-o"></i></a></h3>
                            <div id="regulamento-10-10" class="collapse in" aria-expanded="true">
                                <p>1.1 A campanha Seleção SKF 2017 vai premiar profissionais da área de vendas da rede de Distribuidores Industriais e Automotivos da SKF do Brasil.</p>
                                <br />
                                <p>1.2 São elegíveis a participar da Campanha profissionais de vendas dos distribuidores que preencherem ou reconfirmarem seu cadastro no site <a href="http://www.selecaoskf.com.br">www.selecaoskf.com.br</a> , até o dia 31 de janeiro de 2017 e que também trabalhem em:</p>
                                <p style="padding-left: 5%;">a. Um distribuidor industrial que comprou no mínimo R$ 1,2 milhão em produtos SKF no período de janeiro a dezembro de 2016.</p>
                                <p style="padding-left: 5%;">b. Um distribuidor automotivo que comprou no mínimo R$ 4,8 milhões, sem impostos, de produtos SKF no período de janeiro a dezembro de 2016.</p>
                                <br />
                                <p>1.3 A Campanha Seleção SKF será realizada no período de 01/01/2017 a 31/05/2017.</p>
                                <%--<p style="padding-left: 5%;">1.3.1 Para os Distribuidores Industriais, estamos considerando também, para cálculo do resultado, os valores vendidos em produtos SKF no mês de julho de 2016.</p>--%>
                                <br />
                                <p>1.4 Sobre os prêmios:</p>
                                <p style="padding-left: 5%;">a. Prêmio final – viagem para Gotemburgo na Suécia, no período de 15 a 22 de julho de 2017, para acompanhar a Gothia Cup 2017 (<a href="http://www.gothiacup.se/eng">www.gothiacup.se/eng</a>) e conhecer instalações da SKF.</p>
                                <p style="padding-left: 5%;">b.	Prêmios intermediários – produtos que poderão ser trocados pelos pontos de cada participante, a partir de uma lista pré-determinada que estará disponível no site <a href="http://www.selecaoskf.com.br">www.selecaoskf.com.br</a></p>
                                <p>1.5 Os prêmios serão distribuídos com os seguintes critérios:</p>
                                <p style="padding-left: 5%;">a. Prêmio Final</p>
                                <p style="padding-left: 10%;">i. Distribuidor autorizado industrial</p>
                                <p style="padding-left: 15%;">1. O vendedor do distribuidor que tiver reunido o maior número de pontos durante o período de apuração da campanha, sendo considerado um (1) vendedor para cada distribuidor;</p>
                                <p style="padding-left: 15%;">2. O gerente de vendas do distribuidor que tenha o maior valor em  novos contratos formalizados e que estejam no plano de negócio de clientes da concorrência de 2017. Será considerado apenas um (1)  gerente em toda a rede.</p>
                                <p style="padding-left: 15%;">3. O técnico de campo que tenha o maior valor em Cases de Sucesso, submetidos até 31 de maio de 2017, validados pelos clientes e com autorização de divulgação dos mesmos. Será considerado apenas um (1) técnico em toda a rede, que esteja com todos os níveis de treinamento CMP válidos até 31 de maio de 2017.</p>
                                <p style="padding-left: 10%;">ii. Distribuidor autorizado automotivo </p>
                                <p style="padding-left: 15%;">1. O gerente de filial de distribuidor automotivo que tiver reunido o maior número de pontos durante o período de apuração, sendo considerado um (1) gerente para cada distribuidor;</p>

                                <p style="padding-left: 5%;">b. Prêmios intermediários</p>
                                <p style="padding-left: 10%;">i. Os participantes da campanha poderão escolher produtos que estarão em um catálogo no site www.selecaoskf.com.br, e trocar seus pontos por itens, desde que tenham respondido no mínimo 30 questões do Quiz SKF.</p>
                            </div>
                            <h3><span class="number">2.0</span><span class="text"> Critérios de Pontuação</span><a href="#regulamento-10-11" class="toggle pull-right" role="button" data-toggle="collapse" aria-controls="regulamento-10-11"><i class="fa fa-minus-square-o"></i></a></h3>
                            <div id="regulamento-10-11" class="collapse in" aria-expanded="true">
                                <p>2.1 Distribuidor industrial</p>
                                <p style="padding-left: 5%;">2.1.1 Cada vendedor terá uma meta mensal de vendas de produtos SKF. Esta meta será definida individualmente pela direção de cada distribuidor.</p>
                                <p style="padding-left: 5%;">2.1.2 Os pontos de cada vendedor serão calculados com base no percentual atingido da meta, na proporção de 1% = 1 ponto.</p>
                                <p style="padding-left: 5%;">2.1.3 Exemplo:</p>
                                <table cellspacing="0" cellpadding="0" border="1" class="table" style="padding-left: 5%; margin-left: 5%; width: 90%;">
                                    <col width="64" />
                                    <col width="71" span="2" />
                                    <col width="45" />
                                    <col width="64" />
                                    <tr>
                                        <td width="64">Vendedor</td>
                                        <td width="71">Meta (R$)</td>
                                        <td width="71">Realizado (R$)</td>
                                        <td width="45">% da meta</td>
                                        <td width="64">Pontos</td>
                                    </tr>
                                    <tr>
                                        <td width="64">A</td>
                                        <td width="71">1.000,00</td>
                                        <td width="71">800</td>
                                        <td width="45">80%</td>
                                        <td width="64">80</td>
                                    </tr>
                                    <tr>
                                        <td width="64">B</td>
                                        <td width="71">2.000,00</td>
                                        <td width="71">2.500,00</td>
                                        <td width="45">125%</td>
                                        <td width="64">125</td>
                                    </tr>
                                    <tr>
                                        <td width="64">C</td>
                                        <td width="71">500</td>
                                        <td width="71">1.000,00</td>
                                        <td width="45">200%</td>
                                        <td width="64">200</td>
                                    </tr>
                                </table>
                                <br />
                                <p>2.2 Será disponibilizado, ao longo dos meses testes de conhecimento (quizzes) que valerão pontos para a Campanha.</p>
                                <p style="padding-left: 5%;">2.2.1 A cada pergunta respondida corretamente, o participante ganha um ponto, que será somado ao seu resultado final.</p>
                                <p style="padding-left: 5%;">2.2.2 O conteúdo dos quizzes é baseado nos cursos DD College, que estão disponíveis do DD Community da SKF.</p>
								<p style="padding-left: 5%;">2.2.3 As perguntas dos quizzes estarão disponíveis para serem respondidas das 15h00 às 23h59 do dia em que forem publicadas. Haverá mais duas chances, depois deste período para responder às perguntas: no fim de cada mês, e ao fim da campanha.</p>
                                <br />
                                <p>2.3 Para esta campanha, alguns produtos da linha da SKF serão aceleradores de resultado, funcionando da seguinte maneira:</p>
                                <table border="1" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td width="46">
                                            <p align="center">Meta (R$)</p>
                                        </td>
                                        <td width="70">
                                            <p align="center">Realizado (R$)</p>
                                        </td>
                                        <td width="81">
                                            <p align="center">Valor ( R$) de    produtos Acelerador 1 (x2)</p>
                                        </td>
                                        <td width="94">
                                            <p align="center">Valor (R$ ) de    produtos Acelerador 2  (x1,5)</p>
                                        </td>
                                        <td width="85">
                                            <p align="center">Valor (R$)  de produtos Acelerador 3 (x2)</p>
                                        </td>
                                        <td width="252">
                                            <p align="center">Cálculo    resultado (R$)</p>
                                        </td>
                                        <td width="52">
                                            <p>Pontos</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="46" valign="top">
                                            <p align="right">1.000</p>
                                        </td>
                                        <td width="70" valign="top">
                                            <p align="right">1.500</p>
                                        </td>
                                        <td width="81" valign="top">
                                            <p align="right">200</p>
                                        </td>
                                        <td width="94" valign="top">
                                            <p align="right">100</p>
                                        </td>
                                        <td width="85" valign="top">
                                            <p align="right">200</p>
                                        </td>
                                        <td width="252" valign="top">
                                            <p align="right">1.000+(200*2)+(100*1,5)+(200*2)    = 1.750</p>
                                        </td>
                                        <td width="52" valign="top">
                                            <p align="right">175</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="46" valign="top">
                                            <p align="right">300</p>
                                        </td>
                                        <td width="70" valign="top">
                                            <p align="right">250</p>
                                        </td>
                                        <td width="81" valign="top">
                                            <p align="right">50</p>
                                        </td>
                                        <td width="94" valign="top">
                                            <p align="right">0</p>
                                        </td>
                                        <td width="85" valign="top">
                                            <p align="right">10</p>
                                        </td>
                                        <td width="252" valign="top">
                                            <p align="right">200 +    (50*2)+(0*1,5)+(10*2) = 310</p>
                                        </td>
                                        <td width="52" valign="top">
                                            <p align="right">103</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="46" valign="top">
                                            <p align="right">10.000</p>
                                        </td>
                                        <td width="70" valign="top">
                                            <p align="right">11.000</p>
                                        </td>
                                        <td width="81" valign="top">
                                            <p align="right">500</p>
                                        </td>
                                        <td width="94" valign="top">
                                            <p align="right">300</p>
                                        </td>
                                        <td width="85" valign="top">
                                            <p align="right">100</p>
                                        </td>
                                        <td width="252" valign="top">
                                            <p align="right">10.100 +    (500*2)+ (300*1,5)+ (100*2) = 11.750</p>
                                        </td>
                                        <td width="52" valign="top">
                                            <p align="right">107</p>
                                        </td>
                                    </tr>
                                </table>
                                <p>2.4 Para esta campanha, foram definidos os seguintes produtos como aceleradores de resultados:</p>
                                <p style="padding-left: 5%;">2.4.1 Produtos Mapro – acelerador 2</p>
                                <p style="padding-left: 5%;">2.4.2 Produtos Como – acelerador 1,5</p>
                                <p style="padding-left: 5%;">2.4.3 SRB – acelerador 2</p>
                                <p style="padding-left: 5%;">2.4.4 DGBB – acelerador 2</p>
                                <p style="padding-left: 5%;">2.4.5 Mancais – acelerador 1,5</p>
                                <p style="padding-left: 5%;">2.4.6 Vedações – acelerador 1,5</p>
								<p style="padding-left: 5%;">2.4.7 Produtos de Transmissão e potência – acelerador 2x</p>
                                <br />
                                <p>2.5 O total de pontos de cada participante será a soma de pontos relacionados à meta de vendas - já com o cálculo envolvendo os produtos aceleradores - com os pontos conquistados nos quizzes.</p>
                                <br />
                                <p>2.6 Nota de Corte</p>
                                <p style="padding-left: 5%;">2.6.1 Em cada distribuidora, com base nas metas de vendas definidas, será calculada uma média aritmética simples, das metas de todos os vendedores. Este resultado será a Nota de Corte da distribuidora;</p>
                                <p style="padding-left: 5%;">2.6.2 Só estão aptos a ganhar o prêmio final os vendedores que acumularem no período da campanha,  vendas no valor igual ou superior à nota de corte da sua distribuidora. Para a validação da Nota de Corte serão considerados os valores vendidos, antes da aplicação dos aceleradores.</p>
                                <p style="padding-left: 5%;">2.6.3 Exemplo</p>
                                <p style="padding-left: 10%;">Os vendedores A e C não estão aptos a participar da premiação mensal. Neste caso, o Vendedor E é vencedor do mês.</p>
                                <table cellspacing="0" cellpadding="0" border="1" class="table" style="padding-left: 5%; margin-left: 5%; width: 90%;">
                                    <tbody>
                                        <tr>
                                            <td width="192" colspan="2">
                                                <p align="center">Nota de corte Distribuidor Alfa</p>
                                            </td>
                                            <td width="280" colspan="3">
                                                <p align="center">R$ 300.000,00</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="72">
                                                <p align="center">Vendedor</p>
                                            </td>
                                            <td width="120">
                                                <p align="center">Meta (R$)</p>
                                            </td>
                                            <td width="110">
                                                <p align="center">Realizado (R$)</p>
                                            </td>
                                            <td width="80">
                                                <p align="center">Apto ou não</p>
                                            </td>
                                            <td width="89">
                                                <p align="center">Pontos</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="72" valign="top">
                                                <p>A</p>
                                            </td>
                                            <td width="120" valign="top">
                                                <p align="right">100.000</p>
                                            </td>
                                            <td width="110" valign="top">
                                                <p align="right">150.000</p>
                                            </td>
                                            <td width="80" valign="top">
                                                <p align="center">Não</p>
                                            </td>
                                            <td width="89" valign="top">
                                                <p align="center">150</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="72" valign="top">
                                                <p>B</p>
                                            </td>
                                            <td width="120" valign="top">
                                                <p align="right">200.000</p>
                                            </td>
                                            <td width="110" valign="top">
                                                <p align="right">380.000</p>
                                            </td>
                                            <td width="80" valign="top">
                                                <p align="center">Sim</p>
                                            </td>
                                            <td width="89" valign="top">
                                                <p align="center">190</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="72" valign="top">
                                                <p>C</p>
                                            </td>
                                            <td width="120" valign="top">
                                                <p align="right">500.000</p>
                                            </td>
                                            <td width="110" valign="top">
                                                <p align="right">340.000</p>
                                            </td>
                                            <td width="80" valign="top">
                                                <p align="center">Sim</p>
                                            </td>
                                            <td width="89" valign="top">
                                                <p align="center">68</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="72" valign="top">
                                                <p>D</p>
                                            </td>
                                            <td width="120" valign="top">
                                                <p align="right">600.000</p>
                                            </td>
                                            <td width="110" valign="top">
                                                <p align="right">650.000</p>
                                            </td>
                                            <td width="80" valign="top">
                                                <p align="center">Sim</p>
                                            </td>
                                            <td width="89" valign="top">
                                                <p align="center">108</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="72" valign="top">
                                                <p>E</p>
                                            </td>
                                            <td width="120" valign="top">
                                                <p align="right">100.000</p>
                                            </td>
                                            <td width="110" valign="top">
                                                <p align="right">205.000</p>
                                            </td>
                                            <td width="80" valign="top">
                                                <p align="center">Não</p>
                                            </td>
                                            <td width="89" valign="top">
                                                <p align="center">205</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p style="padding-left: 5%;">2.6.4 Em caso de empate no número de pontos, será escolhido vencedor o vendedor que:</p>
                                <p style="padding-left: 10%;">2.6.4.1 Tiver o maior valor em R$ vendidos em produtos SKF no período da campanha;</p>
                                <p style="padding-left: 10%;">2.6.4.2 Acertado o maior número de perguntas dos quizzes;</p>
                                <p style="padding-left: 10%;">2.6.4.3 Dia e hora de resposta dos quizzes; aquele que responder mais cedo, será o vencedor.</p>
                            </div>

                            <h3><span class="number">3.0</span><span class="text"> Distribuidor automotivo</span><a href="#regulamento-10-12" class="toggle pull-right" role="button" data-toggle="collapse" aria-controls="regulamento-10-12"><i class="fa fa-minus-square-o"></i></a></h3>
                            <div id="regulamento-10-12" class="collapse in" aria-expanded="true">
                                <p>3.1 O gerente terá uma meta mensal de vendas de produtos SKF. Esta meta será definida individualmente pela direção de cada distribuidor.</p>
                                <p>3.2 Os pontos de cada gerente serão calculados com base no percentual atingido da meta, na proporção de 1% = 1 ponto.</p>
                                <p>3.3 Será disponibilizado, ao longo dos meses testes de conhecimento (quizzes) que valerão pontos para a  Campanha.</p>
                                <table border="1" cellspacing="0" cellpadding="0" width="100%;">
                                    <tr>
                                        <td width="110">
                                            <p align="center">Filial</p>
                                        </td>
                                        <td width="88">
                                            <p align="center">Meta (R$)</p>
                                        </td>
                                        <td width="108">
                                            <p align="center">Realizado (R$)</p>
                                        </td>
                                        <td width="89">
                                            <p align="center">% da meta</p>
                                        </td>
                                        <td width="97">
                                            <p align="center">Total</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="110" valign="top">
                                            <p align="center">A</p>
                                        </td>
                                        <td width="88" valign="top">
                                            <p align="right">10.000,00</p>
                                        </td>
                                        <td width="108" valign="top">
                                            <p align="right">8.000,00</p>
                                        </td>
                                        <td width="89" valign="top">
                                            <p align="center">80 %</p>
                                        </td>
                                        <td width="97" valign="top">
                                            <p align="center">80</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="110" valign="top">
                                            <p align="center">B</p>
                                        </td>
                                        <td width="88" valign="top">
                                            <p align="right">20.000,00</p>
                                        </td>
                                        <td width="108" valign="top">
                                            <p align="right">25.000,00</p>
                                        </td>
                                        <td width="89" valign="top">
                                            <p align="center">125 %</p>
                                        </td>
                                        <td width="97" valign="top">
                                            <p align="center">125</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="110" valign="top">
                                            <p align="center">C</p>
                                        </td>
                                        <td width="88" valign="top">
                                            <p align="right">5.000,00</p>
                                        </td>
                                        <td width="108" valign="top">
                                            <p align="right">10.000,00</p>
                                        </td>
                                        <td width="89" valign="top">
                                            <p align="center">200 %</p>
                                        </td>
                                        <td width="97" valign="top">
                                            <p align="center">200</p>
                                        </td>
                                    </tr>
                                </table>
                                <p>3.4 Será disponibilizado, ao longo dos meses testes de conhecimento (quizzes) que valerão pontos para a  Campanha.</p>
                                <p style="padding-left: 5%;">3.4.1 A cada pergunta respondida corretamente, o participante ganha um ponto, que será somado ao seu resultado final.</p>
                                <p>3.5 Para esta campanha, alguns produtos da linha da SKF serão aceleradores de resultado, que adicionarão pontos ao resultado final, em função da quantidade de peças vendidas.</p>
                                <p>3.6 Para esta campanha, foram definidos os seguintes produtos como aceleradores de resultados:</p>
                                <p style="padding-left: 5%;">3.6.1 Rolamentos – cada peça vendida vale 0,04 pontos.</p>
                                <p style="padding-left: 5%;">3.6.2 Atuadores e componentes de embreagem (linha VKCH)  – cada peça vendida vale 0,01 ponto</p>
                                <p style="padding-left: 5%;">3.6.3 Tensionadores e polias (linha VKM) – cada peça vendida vale 0,03 pontos.</p>
                                <p style="padding-left: 5%;">3.6.4 Componentes de suspensão (linha VKDS) – cada peça vendida vale 0,01 ponto</p>
                                <p style="padding-left: 5%;">3.6.5 Componentes de direção (linha VKY) – cada peça vendida vale 0,01 ponto</p>
                                <p>3.7 O total de pontos de cada participante será a soma de pontos relacionados à meta de vendas somados aos pontos por quantidade de peças vendidas e aos pontos referentes às respostas do quiz.</p>
                                <p>3.8 Nota de Corte</p>
                                <p style="padding-left: 5%;">3.8.1 Em cada distribuidora, com base nas metas de vendas definidas, será calculada uma média aritmética simples, das metas de todos os gerentes. Este resultado será a Nota de Corte da distribuidora;</p>
                                <p style="padding-left: 5%;">3.8.2 Só estão aptos a ganhar o prêmio final os gerentes que acumularem no período da campanha,  vendas no valor igual ou superior à nota de corte da sua distribuidora.</p>
                                <p style="padding-left: 5%;">Exemplo:</p>
                                <table border="1" cellspacing="0" cellpadding="0" align="left" width="100%;">
                                    <tr>
                                        <td width="192" colspan="2">
                                            <p align="center">Nota de corte    Distribuidor Alfa</p>
                                        </td>
                                        <td width="280" colspan="4">
                                            <p align="center">R$ 300.000,00</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="120">
                                            <p align="center">Meta (R$)</p>
                                        </td>
                                        <td width="110" colspan="2">
                                            <p align="center">Realizado (R$)</p>
                                        </td>
                                        <td width="80">
                                            <p align="center">Apto ou não</p>
                                        </td>
                                        <td width="89">
                                            <p align="center">Pontos</p>
                                        </td>
                                        <td width="72">
                                            <p>&nbsp;</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="120" valign="top">
                                            <p align="right">100.000</p>
                                        </td>
                                        <td width="110" colspan="2" valign="top">
                                            <p align="right">150.000</p>
                                        </td>
                                        <td width="80" valign="top">
                                            <p align="center">Não </p>
                                        </td>
                                        <td width="89" valign="top">
                                            <p align="center">150</p>
                                        </td>
                                        <td width="72">
                                            <p>&nbsp;</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="120" valign="top">
                                            <p align="right">200.000</p>
                                        </td>
                                        <td width="110" colspan="2" valign="top">
                                            <p align="right">380.000</p>
                                        </td>
                                        <td width="80" valign="top">
                                            <p align="center">Sim </p>
                                        </td>
                                        <td width="89" valign="top">
                                            <p align="center">190</p>
                                        </td>
                                        <td width="72">
                                            <p>&nbsp;</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="120" valign="top">
                                            <p align="right">500.000</p>
                                        </td>
                                        <td width="110" colspan="2" valign="top">
                                            <p align="right">340.000</p>
                                        </td>
                                        <td width="80" valign="top">
                                            <p align="center">Sim </p>
                                        </td>
                                        <td width="89" valign="top">
                                            <p align="center">68</p>
                                        </td>
                                        <td width="72">
                                            <p>&nbsp;</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="120" valign="top">
                                            <p align="right">600.000</p>
                                        </td>
                                        <td width="110" colspan="2" valign="top">
                                            <p align="right">650.000</p>
                                        </td>
                                        <td width="80" valign="top">
                                            <p align="center">Sim </p>
                                        </td>
                                        <td width="89" valign="top">
                                            <p align="center">108</p>
                                        </td>
                                        <td width="72">
                                            <p>&nbsp;</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="120" valign="top">
                                            <p align="right">100.000</p>
                                        </td>
                                        <td width="110" colspan="2" valign="top">
                                            <p align="right">205.000</p>
                                        </td>
                                        <td width="80" valign="top">
                                            <p align="center">Não </p>
                                        </td>
                                        <td width="89" valign="top">
                                            <p align="center">205</p>
                                        </td>
                                        <td width="72">
                                            <p>&nbsp;</p>
                                        </td>
                                    </tr>
                                </table>
                                <p>Em caso de empate no número de pontos, será escolhido vencedor o vendedor que tiver o maior valor em R$ vendidos em produtos SKF no período da campanha;</p>
                            </div>
                            
                            <h3><span class="number">4.0</span><span class="text"> As responsabilidades</span><a href="#regulamento-10-14" class="toggle pull-right" role="button" data-toggle="collapse" aria-controls="regulamento-10-14"><i class="fa fa-minus-square-o"></i></a></h3>
                            <div id="regulamento-10-14" class="collapse in" aria-expanded="true">
                                <br />
                                <p>4.1 Distribuidor Industrial</p>
                                <br />
                                <p style="padding-left: 5%;">4.1.1 Determinar e informar as metas totais de cada vendedor para o período da campanha. Data limite para envio dos dados: 13 de janeiro de 2017, via e-mail para <a href="mailto:contato@selecaoskf.com.br">contato@selecaoskf.com.br</a></p>
                                <p style="padding-left: 5%;">4.1.2 Informar mensalmente os resultados de vendas de cada vendedor participante da campanha, até o terceiro dia útil de cada mês, posterior ao mês apurado, via e-mail para <a href="mailto:contato@selecaoskf.com.br">contato@selecaoskf.com.br</a></p>
                                <br />
                                <p>4.2 Distribuidor automotivo</p>
                                <br />
                                <p style="padding-left: 5%;">4.2.1 Determinar e informar as metas totais de cada gerente / filial para o período da campanha. Data limite para envio dos dados: 13 de janeiro de 2017, via e-mail para <a href="mailto:contato@selecaoskf.com.br">contato@selecaoskf.com.br</a></p>
                                <p style="padding-left: 5%;">4.2.2 Informar mensalmente os resultados de vendas de produtos SKF, de cada gerente  participante da campanha, até o terceiro dia útil de cada mês, posterior ao mês apurado, via e-mail para <a href="mailto:contato@selecaoskf.com.br">contato@selecaoskf.com.br</a></p>
                                <br />
                                <p>4.3 SKF</p>
                                <br />
                                <p style="padding-left: 5%;">4.3.1 Criar e manter sistema de acompanhamento, controle e informação sobre o andamento da campanha.</p>
                                <p style="padding-left: 5%;">4.3.2 Providenciar os prêmios intermediários e prêmios finais para os participantes que cumprirem as condições deste regulamento.</p>
                                <p style="padding-left: 5%;">4.3.3 Divulgar os vencedores mensais até o quinto dia útil de cada mês, e posterior entrega dos prêmios.</p>
                                <p style="padding-left: 5%;">4.3.4 Aquisição de todos os prêmios da campanha.</p>
                                <p style="padding-left: 5%;">4.3.5 Acompanhar os vencedores dos prêmios finais durante a viagem.</p>
                                <p style="padding-left: 5%;">4.3.6 Não divulgar, entre a rede, os valores apurados por cada distribuidor.</p>
                            </div>
                            <h3><span class="number">5.0</span><span class="text"> Outras informações</span><a href="#regulamento-10-15" class="toggle pull-right" role="button" data-toggle="collapse" aria-controls="regulamento-10-15"><i class="fa fa-minus-square-o"></i></a></h3>
                            <div id="regulamento-10-15" class="collapse in" aria-expanded="true">
                                <p>5.1 A SKF não se responsabiliza por:</p>
                                <p style="padding-left: 5%;">5.1.1 Transporte da residência do vencedor até o local de embarque para a viagem para a Suécia.</p>
                                <p style="padding-left: 5%;">5.1.2 Compras particulares durante o período da viagem.</p>
                                <p style="padding-left: 5%;">5.1.3 Custo de emissão de passaportes.</p>
                                <p>5.2 A divulgação dos resultados dessa campanha será realizada através do site da campanha em 12 de junho de 2017.</p>
                                <p>5.3 Serão desclassificados os vendedores e gerentes que agirem em desacordo com as regras estabelecidas neste regulamento, inclusive através de atos fraudulentos, sendo de plena responsabilidade deste, todo e qualquer dano causado pela eventual prática de atos ilícitos.</p>
                                <p>5.4 Apenas serão elegíveis à presente campanha os vendedores e gerente que estiverem efetivos e ativos em todo o período compreendido por esta campanha, não podendo estar em cumprimento de aviso prévio, tampouco ser funcionário temporário</p>
                                <p>5.5 Não será atribuída a premiação no caso de desligamento (voluntário e/ou forçado) do vendedor ou gerente do quadro de empregados dos distribuidores industriais ou automotivos, mesmo que este já esteja cadastrado na Campanha.</p>
                                <p>5.6 Caso o vencedor da campanha esteja impossibilitado de viajar no período estipulado, fica a critério da direção de cada distribuidor definir quem será a pessoa que fará a viagem.</p>
                                <p>5.7 Em hipótese alguma o prêmio será convertido em valores monetários, ou descontos para aquisição de produtos SKF.</p>
                                <p>5.8 Caso algum distribuidor participante não tenha um funcionário apto para a viagem, a SKF está desobrigada de entregar este prêmio para este distribuidor específico.</p>
                                <p>5.9 Os participantes da campanha poderão enviar suas dúvidas e sugestões através do site da campanha, via e-mail <a href="mailto:contato@selecaoskf.com.br">contato@selecaoskf.com.br</a> ou por meio de Whatsapp (11) 9.7470.2952</p>
                                <p>5.10 Os casos omissos, dúvidas ou divergências serão julgados por uma comissão formada por funcionários da SKF do Brasil cujas decisões serão soberanas e irrecorríveis.</p>
                                <p>5.11 Os participantes da presente campanha autorizam, desde já, o uso de sua imagem, som de voz e nome na internet, em filmes, vídeos, fotos e cartazes, anúncios em jornais, revistas e televisão, toda mídia impressa ou eletrônica, pelo prazo de 01 (um) ano, a contar da data do término desta, para eventuais comunicações desta Campanha de Incentivo, sem qualquer ônus para a SKF do Brasil</p>
                                <p>5.12 A participação desta campanha implica no conhecimento e aceitação total e irrestrita de todos os termos e condições deste Regulamento.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
