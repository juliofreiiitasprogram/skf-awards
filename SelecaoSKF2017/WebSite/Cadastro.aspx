﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cadastro.aspx.cs" Inherits="WebSite.Cadastro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cadastro Seleção SKF</title>
    <link href="assets/css/style.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="Scripts/jquery-ui-1.8.24.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.interactive_bg.js"></script>
    <script src="assets/js/mascara.js"></script>
    <script type="text/javascript">
        function retornaEndereco() {
            var cep = document.getElementById("TxtCep").value.replace(/\D/g, "");
            //var cep = document.getElementById("txtCEP").value;
            if (!(/^[0-9]{8}$/.test(cep))) return;

            var xmlhttp;
            if (window.XMLHttpRequest)
                xmlhttp = new XMLHttpRequest();
            else
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState === 4 && xmlhttp.status === 200)
                    (function (response) {
                        console.log(response);
                        var logradouro = JSON.parse(response)["logradouro"];
                        var bairro = JSON.parse(response)["bairro"];
                        var localidade = JSON.parse(response)["localidade"];
                        var uf = JSON.parse(response)["uf"];

                        var txtEndereco = document.getElementById("TxtEndereco");
                        var txtBairro = document.getElementById("TxtBairro");
                        var txtCidade = document.getElementById("TxtCidade");
                        var txtUF = document.getElementById("TxtUF");
                        var txtNumero = document.getElementById("TxtNumero");

                        var hfCEP2 = document.getElementById("hfCEP2");
                        var hfEndereco2 = document.getElementById("hfEndereco2");
                        var hfBairro2 = document.getElementById("hfBairro2");
                        var hfCidade2 = document.getElementById("hfCidade2");
                        var hfUF2 = document.getElementById("hfUF2");

                        if (typeof logradouro === "undefined") {
                            $('#myCep').modal('show');
                            txtEndereco.value = "";
                            txtBairro.value = "";
                            txtCidade.value = "";
                            txtUF.value = "";
                            txtNumero.value = "";

                            hfCEP2.value = "";
                            hfEndereco2.value = "";
                            hfBairro2.value = "";
                            hfCidade2.value = "";
                            hfUF2.value = "";
                        }
                        else {
                            TxtEndereco.value = logradouro;
                            TxtBairro.value = bairro;
                            TxtCidade.value = localidade;
                            TxtUF.value = uf;

                            hfCEP2.value = logradouro;
                            hfEndereco2.value = logradouro;
                            hfBairro2.value = bairro;
                            hfCidade2.value = localidade;
                            hfUF2.value = uf;

                            txtNumero.classList.add("attention");
                        };
                    }(xmlhttp.responseText));
            }

            xmlhttp.open("GET", "https://viacep.com.br/ws/" + cep + "/json/", true);

            xmlhttp.setRequestHeader("Content-Type", "application/json;charset=utf-8");

            xmlhttp.send();
        }

    </script>
    <style>
        table td {
            padding-left: 2%;
            padding-right: 2%;
        }
    </style>
</head>
<body class="no-animate">
    <form id="form1" runat="server">
        <div id="site">
            <header id="header">
                <div class="component">
                    <div id="slider" class="no-effects">
                        <div class="context">
                            <div class="logo">
                                <a href="#slider-toggle" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="slider-toggle">
                                    <img src="assets/img/logo.png" alt="" width="142" height="215"></a>
                            </div>
                            <div id="slider-toggle" class="text collapse">
                                <div class="middle">
                                    <div class="box">
                                        <div class="content">
                                            <div class="title"></div>
                                            <div class="subtitle">A torcida da seleção skf só aumenta!</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="image">
                            <div class="bg-1"></div>
                            <div class="bg-2"></div>
                            <div class="bg-3"></div>
                        </div>
                    </div>
                    <div id="menu">
                        <nav class="navbar navbar-default" role="navigation">
                            <div class="container">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="#"></a>
                                </div>

                                <!-- /.navbar-collapse -->
                            </div>
                            <!-- /.container-fluid -->
                        </nav>
                    </div>
                    <div id="logo-line">
                        <div class="container-fluid">
                            <div class="line row">
                                <div class="pull-right">
                                    <div class="logo"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div id="page">
                <div class="component">
                    <div class="container">
                        <div class="row">
                            <div id="contato" class="col-xs-12 space simple-page">
                                <div id="modalRegulamento" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                    <div class="modal-dialog modal-lg" role="document">

                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="gridSystemModalLabel"><strong style="color: #000;">Regulamento da campanha</strong></h4>
                                            </div>
                                            <div class="modal-body" style="color: #808080;">
                                                <h2>A campanha</h2>
                                                <p>1.	A campanha Seleção SKF 2017 vai premiar profissionais da área de vendas da rede de Distribuidores Industriais e Automotivos da SKF do Brasil.</p>
                                                <p>2.	São elegíveis a participar da Campanha profissionais de vendas dos distribuidores que preencherem ou reconfirmarem seu cadastro no site www.selecaoskf.com.br , até o dia 31 de janeiro de 2017 e que também trabalhem em:</p>
                                                <p>a.	Um distribuidor industrial que comprou no mínimo R$ 1,2 milhão em produtos SKF no período de janeiro a dezembro de 2016.</p>
                                                <p>b.	Um distribuidor automotivo que comprou no mínimo R$ 4,8 milhões, sem impostos, de produtos SKF no período de janeiro a dezembro de 2016.</p>
                                                <p>3.	A Campanha Seleção SKF será realizada no período de 01/01/2017 a 31/05/2017.</p>
                                                <p>4.	Sobre os prêmios</p>
                                                <p>a.	Prêmio final – viagem para Gotemburgo na Suécia, no período de 15 a 22 de julho de 2017, para acompanhar a Gothia Cup 2017 (www.gothiacup.se/eng) e conhecer instalações da SKF.</p>
                                                <p>b.	Prêmios intermediários – produtos que poderão ser trocados pelos pontos de cada participante, a partir de uma lista pré-determinada que estará disponível no site www.selecaoskf.com.br</p>
                                                <p>5.	Os prêmios serão distribuídos com os seguintes critérios:</p>
                                                <p>a.	Prêmio Final</p>
                                                <p>i.	Distribuidor autorizado industrial</p>
                                                <p>1.	O vendedor do distribuidor que tiver reunido o maior número de pontos durante o período de apuração da campanha, sendo considerado um (1) vendedor para cada distribuidor;</p>
                                                <p>2.	O gerente de vendas do distribuidor que tenha o maior valor em  novos contratos formalizados e que estejam no plano de negócio de clientes da concorrência de 2017. Será considerado apenas um (1)  gerente em toda a rede.</p>
                                                <p>3.	O técnico de campo que tenha o maior valor em Cases de Sucesso, submetidos até 31 de maio de 2017, validados pelos clientes e com autorização de divulgação dos mesmos. Será considerado apenas um (1) técnico em toda a rede, que esteja com todos os níveis de treinamento CMP válidos até 31 de maio de 2017.</p>
                                                <p>ii.	Distribuidor autorizado automotivo </p>
                                                <p>1.	O gerente de filial de distribuidor automotivo que tiver reunido o maior número de pontos durante o período de apuração, sendo considerado um (1) gerente para cada distribuidor;</p>
                                                <p>b.	Prêmios intermediários</p>
                                                <p>i.	Os participantes da campanha poderão escolher produtos que estarão em um catálogo no site www.selecaoskf.com.br , e trocar seus pontos por itens, desde que tenham respondido no mínimo 30 questões do Quiz SKF.</p>
                                                <p>6.	Critérios de Pontuação</p>
                                                <p>a.	Distribuidor industrial</p>
                                                <p>i.	Cada vendedor terá uma meta mensal de vendas de produtos SKF. Esta meta será definida individualmente pela direção de cada distribuidor.</p>
                                                <p>ii.	Os pontos de cada vendedor serão calculados com base no percentual atingido da meta, na proporção de 1% = 1 ponto.</p>
                                                <p>iii.	Exemplo:</p>
                                                <table border="1" cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td width="110" valign="top">
                                                            <p align="center">Vendedor</p>
                                                        </td>
                                                        <td width="88" valign="top">
                                                            <p>Meta (R$) </p>
                                                        </td>
                                                        <td width="108" valign="top">
                                                            <p>Realizado (R$)</p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p>% da meta</p>
                                                        </td>
                                                        <td width="77" valign="top">
                                                            <p>Pontos</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="110" valign="top">
                                                            <p align="center">A</p>
                                                        </td>
                                                        <td width="88" valign="top">
                                                            <p align="right">1.000,00</p>
                                                        </td>
                                                        <td width="108" valign="top">
                                                            <p align="right">800,00</p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">80 %</p>
                                                        </td>
                                                        <td width="77" valign="top">
                                                            <p align="center">80</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="110" valign="top">
                                                            <p align="center">B</p>
                                                        </td>
                                                        <td width="88" valign="top">
                                                            <p align="right">2.000,00</p>
                                                        </td>
                                                        <td width="108" valign="top">
                                                            <p align="right">2.500,00</p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">125 %</p>
                                                        </td>
                                                        <td width="77" valign="top">
                                                            <p align="center">125</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="110" valign="top">
                                                            <p align="center">C</p>
                                                        </td>
                                                        <td width="88" valign="top">
                                                            <p align="right">500,00</p>
                                                        </td>
                                                        <td width="108" valign="top">
                                                            <p align="right">1.000,00</p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">200 %</p>
                                                        </td>
                                                        <td width="77" valign="top">
                                                            <p align="center">200</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <p>iv.	Será disponibilizado, ao longo dos meses testes de conhecimento (quizzes) que valerão pontos para a  Campanha.</p>
                                                <p>1.	A cada pergunta respondida corretamente, o participante ganha um ponto, que será somado ao seu resultado final.</p>
                                                <p>2.	O conteúdo dos quizzes é baseado nos cursos DD College, que estão disponíveis do DD Community da SKF.</p>
                                                <p>v.	Para esta campanha, alguns produtos da linha da SKF serão aceleradores de resultado, funcionando da seguinte maneira:</p>
                                                <table border="1" cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td width="46">
                                                            <p align="center">Meta (R$)</p>
                                                        </td>
                                                        <td width="70">
                                                            <p align="center">Realizado (R$)</p>
                                                        </td>
                                                        <td width="81">
                                                            <p align="center">Valor ( R$) de    produtos Acelerador 1 (x2)</p>
                                                        </td>
                                                        <td width="94">
                                                            <p align="center">Valor (R$ ) de    produtos Acelerador 2  (x1,5)</p>
                                                        </td>
                                                        <td width="85">
                                                            <p align="center">Valor (R$)  de produtos Acelerador 3 (x2)</p>
                                                        </td>
                                                        <td width="252">
                                                            <p align="center">Cálculo    resultado (R$)</p>
                                                        </td>
                                                        <td width="52">
                                                            <p>Pontos</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="46" valign="top">
                                                            <p align="right">1.000</p>
                                                        </td>
                                                        <td width="70" valign="top">
                                                            <p align="right">1.500</p>
                                                        </td>
                                                        <td width="81" valign="top">
                                                            <p align="right">200</p>
                                                        </td>
                                                        <td width="94" valign="top">
                                                            <p align="right">100</p>
                                                        </td>
                                                        <td width="85" valign="top">
                                                            <p align="right">200</p>
                                                        </td>
                                                        <td width="252" valign="top">
                                                            <p align="right">1.000+(200*2)+(100*1,5)+(200*2)    = 1.750</p>
                                                        </td>
                                                        <td width="52" valign="top">
                                                            <p align="right">175</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="46" valign="top">
                                                            <p align="right">300</p>
                                                        </td>
                                                        <td width="70" valign="top">
                                                            <p align="right">250</p>
                                                        </td>
                                                        <td width="81" valign="top">
                                                            <p align="right">50</p>
                                                        </td>
                                                        <td width="94" valign="top">
                                                            <p align="right">0</p>
                                                        </td>
                                                        <td width="85" valign="top">
                                                            <p align="right">10</p>
                                                        </td>
                                                        <td width="252" valign="top">
                                                            <p align="right">200 +    (50*2)+(0*1,5)+(10*2) = 310</p>
                                                        </td>
                                                        <td width="52" valign="top">
                                                            <p align="right">103</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="46" valign="top">
                                                            <p align="right">10.000</p>
                                                        </td>
                                                        <td width="70" valign="top">
                                                            <p align="right">11.000</p>
                                                        </td>
                                                        <td width="81" valign="top">
                                                            <p align="right">500</p>
                                                        </td>
                                                        <td width="94" valign="top">
                                                            <p align="right">300</p>
                                                        </td>
                                                        <td width="85" valign="top">
                                                            <p align="right">100</p>
                                                        </td>
                                                        <td width="252" valign="top">
                                                            <p align="right">10.100 +    (500*2)+ (300*1,5)+ (100*2) = 11.750</p>
                                                        </td>
                                                        <td width="52" valign="top">
                                                            <p align="right">107</p>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <p>vi.	Para esta campanha, foram definidos os seguintes produtos como aceleradores de resultados:</p>
                                                <p>1.	Produtos Mapro – acelerador 2</p>
                                                <p>2.	Produtos Como – acelerador 1,5</p>
                                                <p>3.	SRB – acelerador 2</p>
                                                <p>4.	DGBB – acelerador 2</p>
                                                <p>5.	Mancais – acelerador 1,5</p>
                                                <p>6.	ACBB – acelerador 1,5</p>
                                                <p>vii.	O total de pontos de cada participante será a soma de pontos relacionados à meta de vendas - já com o cálculo envolvendo os produtos aceleradores -  com os pontos conquistados nos quizzes. </p>
                                                <p>viii.	Nota de Corte</p>
                                                <p>1.	Em cada distribuidora, com base nas metas de vendas definidas, será calculada uma média aritmética simples, das metas de todos os vendedores. Este resultado será a Nota de Corte da distribuidora;</p>
                                                <p>2.	Só estão aptos a ganhar o prêmio final os vendedores que acumularem no período da campanha,  vendas no valor igual ou superior à nota de corte da sua distribuidora. Para a validação da Nota de Corte serão considerados os valores vendidos, antes da aplicação dos aceleradores. </p>
                                                <p>3.	Exemplo</p>
                                                <table border="1" cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td width="192" colspan="2">
                                                            <p align="center">Nota de corte    Distribuidor Alfa</p>
                                                        </td>
                                                        <td width="280" colspan="3">
                                                            <p align="center">R$ 300.000,00</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="72">
                                                            <p align="center">Vendedor</p>
                                                        </td>
                                                        <td width="120">
                                                            <p align="center">Meta (R$)</p>
                                                        </td>
                                                        <td width="110">
                                                            <p align="center">Realizado (R$)</p>
                                                        </td>
                                                        <td width="80">
                                                            <p align="center">Apto ou não</p>
                                                        </td>
                                                        <td width="89">
                                                            <p align="center">Pontos</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="72" valign="top">
                                                            <p>A</p>
                                                        </td>
                                                        <td width="120" valign="top">
                                                            <p align="right">100.000</p>
                                                        </td>
                                                        <td width="110" valign="top">
                                                            <p align="right">150.000</p>
                                                        </td>
                                                        <td width="80" valign="top">
                                                            <p align="center">Não </p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">150</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="72" valign="top">
                                                            <p>B</p>
                                                        </td>
                                                        <td width="120" valign="top">
                                                            <p align="right">200.000</p>
                                                        </td>
                                                        <td width="110" valign="top">
                                                            <p align="right">380.000</p>
                                                        </td>
                                                        <td width="80" valign="top">
                                                            <p align="center">Sim </p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">190</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="72" valign="top">
                                                            <p>C</p>
                                                        </td>
                                                        <td width="120" valign="top">
                                                            <p align="right">500.000</p>
                                                        </td>
                                                        <td width="110" valign="top">
                                                            <p align="right">340.000</p>
                                                        </td>
                                                        <td width="80" valign="top">
                                                            <p align="center">Sim </p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">68</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="72" valign="top">
                                                            <p>D</p>
                                                        </td>
                                                        <td width="120" valign="top">
                                                            <p align="right">600.000</p>
                                                        </td>
                                                        <td width="110" valign="top">
                                                            <p align="right">650.000</p>
                                                        </td>
                                                        <td width="80" valign="top">
                                                            <p align="center">Sim </p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">108</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="72" valign="top">
                                                            <p>E</p>
                                                        </td>
                                                        <td width="120" valign="top">
                                                            <p align="right">100.000</p>
                                                        </td>
                                                        <td width="110" valign="top">
                                                            <p align="right">205.000</p>
                                                        </td>
                                                        <td width="80" valign="top">
                                                            <p align="center">Não </p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">205</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <p>4.	Em caso de empate no número de pontos, será escolhido vencedor o vendedor que:</p>
                                                <p>a.	Tiver o maior valor em R$ vendidos em produtos SKF no período da campanha;</p>
                                                <p>b.	Acertado o maior número de perguntas dos quizzes;</p>
                                                <p>c.	Dia e hora de resposta dos quizzes; aquele que responder mais cedo, será o vencedor.</p>
                                                <p>b.	Distribuidor automotivo</p>
                                                <p>i.	Gerente terá uma meta mensal de vendas de produtos SKF. Esta meta será definida individualmente pela direção de cada distribuidor.</p>

                                                <p>ii.	Os pontos de cada gerente serão calculados com base no percentual atingido da meta, na proporção de 1% = 1 ponto.</p>
                                                <p>iii.	Além da pontuação da meta serão somados 5 pontos extras a cada R$ 10.000,00 vendido por cada gerente</p>
                                                <table border="1" cellspacing="0" cellpadding="0" width="100%;">
                                                    <tr>
                                                        <td width="110">
                                                            <p align="center">Filial</p>
                                                        </td>
                                                        <td width="88">
                                                            <p align="center">Meta (R$)</p>
                                                        </td>
                                                        <td width="108">
                                                            <p align="center">Realizado (R$)</p>
                                                        </td>
                                                        <td width="89">
                                                            <p align="center">% da meta</p>
                                                        </td>
                                                        <td width="56">
                                                            <p align="center">Pontos extras</p>
                                                        </td>
                                                        <td width="97">
                                                            <p align="center">Total</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="110" valign="top">
                                                            <p align="center">A</p>
                                                        </td>
                                                        <td width="88" valign="top">
                                                            <p align="right">10.000,00</p>
                                                        </td>
                                                        <td width="108" valign="top">
                                                            <p align="right">8.000,00</p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">80 %</p>
                                                        </td>
                                                        <td width="56" valign="top">
                                                            <p align="center">0</p>
                                                        </td>
                                                        <td width="97" valign="top">
                                                            <p align="center">80</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="110" valign="top">
                                                            <p align="center">B</p>
                                                        </td>
                                                        <td width="88" valign="top">
                                                            <p align="right">20.000,00</p>
                                                        </td>
                                                        <td width="108" valign="top">
                                                            <p align="right">25.000,00</p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">125 %</p>
                                                        </td>
                                                        <td width="56" valign="top">
                                                            <p align="center">10</p>
                                                        </td>
                                                        <td width="97" valign="top">
                                                            <p align="center">135</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="110" valign="top">
                                                            <p align="center">C</p>
                                                        </td>
                                                        <td width="88" valign="top">
                                                            <p align="right">5.000,00</p>
                                                        </td>
                                                        <td width="108" valign="top">
                                                            <p align="right">10.000,00</p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">200 %</p>
                                                        </td>
                                                        <td width="56" valign="top">
                                                            <p align="center">5</p>
                                                        </td>
                                                        <td width="97" valign="top">
                                                            <p align="center">205</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <p>iv.	Será disponibilizado, ao longo dos meses testes de conhecimento (quizzes) que valerão pontos para a  Campanha.</p>
                                                <p>1.	A cada pergunta respondida corretamente, o participante ganha um ponto, que será somado ao seu resultado final.</p>
                                                <p>v.	Para esta campanha, alguns produtos da linha da SKF serão aceleradores de resultado, que adicionarão pontos ao resultado final, em função da quantidade de peças vendidas.</p>
                                                <p>vi.	Para esta campanha, foram definidos os seguintes produtos como aceleradores de resultados:</p>
                                                <p>1.	Atuadores e componentes de embreagem (linha VKCH)  – cada peça vendida vale 0,01 ponto</p>
                                                <p>2.	Tensionadores e polias (linha VKM) – cada peça vendida vale 0,01 ponto</p>
                                                <p>3.	Bomba d’água e reparo (linha VKPC) – cada peça vendida vale 0,01 ponto</p>
                                                <p>4.	Componentes de suspensão (linha VKDS) – cada peça vendida vale 0,01 ponto </p>
                                                <p>5.	Componentes de direção (linha VKY) – cada peça vendida vale 0,01 ponto</p>
                                                <p>6.	Homocinéticas (linhas VKJC e VKJA) – cada peça vendida vale 0,03 pontos</p>
                                                <p>vii.	O total de pontos de cada participante será a soma de pontos relacionados à meta de vendas, somado aos pontos extras, aos pontos por quantidade de peças vendidas e os pontos referentes às respostas do quiz.</p>
                                                <p>viii.	Nota de Corte</p>
                                                <p>1.	Em cada distribuidora, com base nas metas de vendas definidas, será calculada uma média aritmética simples, das metas de todos os gerentes. Este resultado será a Nota de Corte da distribuidora;</p>
                                                <p>2.	Só estão aptos a ganhar o prêmio final os gerentes que acumularem no período da campanha,  vendas no valor igual ou superior à nota de corte da sua distribuidora. Exemplo</p>
                                                <table border="1" cellspacing="0" cellpadding="0" width="100%;">
                                                    <tr>
                                                        <td width="192" colspan="2">
                                                            <p align="center">Nota de corte    Distribuidor Alfa</p>
                                                        </td>
                                                        <td width="280" colspan="3">
                                                            <p align="center">R$ 300.000,00</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="72">
                                                            <p align="center">Vendedor</p>
                                                        </td>
                                                        <td width="120">
                                                            <p align="center">Meta (R$)</p>
                                                        </td>
                                                        <td width="110">
                                                            <p align="center">Realizado (R$)</p>
                                                        </td>
                                                        <td width="80">
                                                            <p align="center">Apto ou não</p>
                                                        </td>
                                                        <td width="89">
                                                            <p align="center">Pontos</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="72" valign="top">
                                                            <p>A</p>
                                                        </td>
                                                        <td width="120" valign="top">
                                                            <p align="right">100.000</p>
                                                        </td>
                                                        <td width="110" valign="top">
                                                            <p align="right">150.000</p>
                                                        </td>
                                                        <td width="80" valign="top">
                                                            <p align="center">Não </p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">150</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="72" valign="top">
                                                            <p>B</p>
                                                        </td>
                                                        <td width="120" valign="top">
                                                            <p align="right">200.000</p>
                                                        </td>
                                                        <td width="110" valign="top">
                                                            <p align="right">380.000</p>
                                                        </td>
                                                        <td width="80" valign="top">
                                                            <p align="center">Sim </p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">190</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="72" valign="top">
                                                            <p>C</p>
                                                        </td>
                                                        <td width="120" valign="top">
                                                            <p align="right">500.000</p>
                                                        </td>
                                                        <td width="110" valign="top">
                                                            <p align="right">340.000</p>
                                                        </td>
                                                        <td width="80" valign="top">
                                                            <p align="center">Sim </p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">68</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="72" valign="top">
                                                            <p>D</p>
                                                        </td>
                                                        <td width="120" valign="top">
                                                            <p align="right">600.000</p>
                                                        </td>
                                                        <td width="110" valign="top">
                                                            <p align="right">650.000</p>
                                                        </td>
                                                        <td width="80" valign="top">
                                                            <p align="center">Sim </p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">108</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="72" valign="top">
                                                            <p>E</p>
                                                        </td>
                                                        <td width="120" valign="top">
                                                            <p align="right">100.000</p>
                                                        </td>
                                                        <td width="110" valign="top">
                                                            <p align="right">205.000</p>
                                                        </td>
                                                        <td width="80" valign="top">
                                                            <p align="center">Não </p>
                                                        </td>
                                                        <td width="89" valign="top">
                                                            <p align="center">205</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <p>a.	Em caso de empate no número de pontos, será escolhido vencedor o vendedor que tiver o maior valor em R$ vendidos em produtos SKF no período da campanha;</p>
                                                <p>As responsabilidades</p>
                                                <p>1.	Distribuidor Industrial</p>
                                                <p>a.	Determinar e informar as metas totais de cada vendedor para o período da campanha. Data limite para envio dos dados: 13 de janeiro de 2017, via e-mail para contato@selecaoskf.com.br</p>
                                                <p>b.	Informar mensalmente os resultados de vendas de cada vendedor participante da campanha, até o terceiro dia útil de cada mês, posterior ao mês apurado, via e-mail para contato@selecaoskf.com.br</p>
                                                <p>2.	Distribuidor automotivo</p>
                                                <p>a.	Determinar e informar as metas totais de cada gerente / filial para o período da campanha. Data limite para envio dos dados: 13 de janeiro de 2017, via e-mail para contato@selecaoskf.com.br</p>
                                                <p>b.	Informar mensalmente os resultados de vendas de produtos SKF, de cada gerente  participante da campanha, até o terceiro dia útil de cada mês, posterior ao mês apurado, via e-mail para contato@selecaoskf.com.br</p>
                                                <p>3.	SKF</p>
                                                <p>a.	Criar e manter sistema de acompanhamento, controle e informação sobre o andamento da campanha.</p>
                                                <p>b.	Providenciar os prêmios intermediários e prêmios finais para os participantes que cumprirem as condições deste regulamento.</p>
                                                <p>c.	Divulgar os vencedores mensais até o quinto dia útil de cada mês, e posterior entrega dos prêmios.</p>
                                                <p>d.	Aquisição de todos os prêmios da campanha.</p>
                                                <p>e.	Acompanhar os vencedores dos prêmios finais durante a viagem.</p>
                                                <p>f.	Não divulgar, entre a rede, os valores apurados por cada distribuidor.</p>

                                                <h2>Outras informações</h2>
                                                <p>1.	A SKF não se responsabiliza por:</p>
                                                <p>a.	Transporte da residência do vencedor até o local de embarque para a viagem para a Suécia.</p>
                                                <p>b.	Compras particulares durante o período da viagem.</p>
                                                <p>c.	Custo de emissão de passaportes.</p>
                                                <p>2.	A divulgação dos resultados dessa campanha será realizada através do site da campanha em 12 de junho de 2017.</p>
                                                <p>3.	Serão desclassificados os vendedores e gerentes que agirem em desacordo com as regras estabelecidas neste regulamento, inclusive através de atos fraudulentos, sendo de plena responsabilidade deste, todo e qualquer dano causado pela eventual prática de atos ilícitos.</p>
                                                <p>4.	Apenas serão elegíveis à presente campanha os vendedores e gerente que estiverem efetivos e ativos em todo o período compreendido por esta campanha, não podendo estar em cumprimento de aviso prévio, tampouco ser funcionário temporário.</p>
                                                <p>5.	Não será atribuída a premiação no caso de desligamento (voluntário e/ou forçado) do vendedor ou gerente do quadro de empregados dos distribuidores industriais ou automotivos, mesmo que este já esteja cadastrado na Campanha.</p>
                                                <p>6.	Caso o vencedor da campanha esteja impossibilitado de viajar no período estipulado, fica a critério da direção de cada distribuidor definir quem será a pessoa que fará a viagem.</p>
                                                <p>7.	Em hipótese alguma o prêmio será convertido em valores monetários, ou descontos para aquisição de produtos SKF.</p>
                                                <p>8.	Caso algum distribuidor participante não tenha um funcionário apto para a viagem, a SKF está desobrigada de entregar este prêmio para este distribuidor específico.</p>
                                                <p>9.	Os participantes da campanha poderão enviar suas dúvidas e sugestões através do site da campanha, via e-mail contato@selecaoskf.com.br ou por meio de Whatsapp (11) 9.7470.2952</p>
                                                <p>10.	Os casos omissos, dúvidas ou divergências serão julgados por uma comissão formada por funcionários da SKF do Brasil cujas decisões serão soberanas e irrecorríveis.</p>
                                                <p>11.	Os participantes da presente campanha autorizam, desde já, o uso de sua imagem, som de voz e nome na internet, em filmes, vídeos, fotos e cartazes, anúncios em jornais, revistas e televisão, toda mídia impressa ou eletrônica, pelo prazo de 01 (um) ano, a contar da data do término desta, para eventuais comunicações desta Campanha de Incentivo, sem qualquer ônus para a SKF do Brasil</p>
                                                <p>12.	A participação desta campanha implica no conhecimento e aceitação total e irrestrita de todos os termos e condições deste Regulamento.</p>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="modalErroRegulamento" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="H1"><strong style="color: #000;">Ops...</strong></h4>
                                            </div>
                                            <div class="modal-body" style="color: #808080;">
                                                Você não aceitou o regulamento da campanha.
                                                <br />
                                                Sem o seu aceite, você não pode continuar.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="modalEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel"></h4>
                                            </div>
                                            <div class="modal-body" style="color:#808080;">
                                                E-mail informado já foi cadastrado.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="body">
                                        <div class="post-content">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-6">
                                                    <div id="AlertaSucesso" class="alert alert-success alert-dismissible alert-fixed" runat="server" visible="false" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <strong>Alteração efetuada com sucesso!!</strong>
                                                    </div>
                                                    <div id="AlertaJaCadastrado" class="alert alert-danger alert-dismissible alert-fixed" runat="server" visible="false" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <strong>Você já esta cadastrado no sistema!!</strong>
                                                    </div>
                                                    <div id="CadastradoSucesso" class="alert alert-success alert-dismissible alert-fixed" runat="server" visible="false" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <strong>Cadastro efetuado com sucesso!!</strong>
                                                    </div>
                                                    <div class="head">
                                                        <div class="title">
                                                            <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-9">
                                                            <label for="LblDistribuidora">Distribuidora / SKF<span class="required">*</span></label>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <asp:DropDownList ID="DdlDistribuidora" OnSelectedIndexChanged="DdlDistribuidora_SelectedIndexChanged" class="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                                                            <asp:Label ID="LblDistribuidora" CssClass="message-error" Visible="false" runat="server" Text="Informe a Distribuidora!!!"></asp:Label>
                                                            <p class="help-block">Selecione sua Distribuidora.</p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-8 text-left">
                                                            <label for="TxtNome">Nome Completo<span class="required">*</span></label>
                                                        </div>
                                                        <div class="col-xs-8">
                                                            <asp:TextBox runat="server" CssClass="form-control" required="required" ID="TxtNome"></asp:TextBox>
                                                            <p class="help-block">Insira o seu nome completo.</p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-4">
                                                            <asp:DropDownList ID="DdlSexo" class="form-control" runat="server" Width="100%">
                                                                <asp:ListItem Value="0" Text="Selecione">Sexo *</asp:ListItem>
                                                                <asp:ListItem Value="Feminino" Text="Feminino">Feminino</asp:ListItem>
                                                                <asp:ListItem Value="Masculino" Text="Masculino">Masculino</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:Label ID="LblSexo" CssClass="message-error" Visible="false" runat="server" Text="Informe o Sexo!!!"></asp:Label>

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-12  text-left">
                                                            <label for="nome">Seu E-mail cadastrado na Distribuidora<span class="required">*</span></label>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <asp:TextBox ID="TxtEmail" class="form-control" required="required" runat="server"></asp:TextBox>
                                                            <asp:Label ID="LblEmail" CssClass="message-error" Visible="false" runat="server" Text="Email invalido!!"></asp:Label>
                                                            <p class="help-block">Insira o seu e-mail para retornar o contato.</p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-8  text-left">
                                                            <label for="nome">Celular</label>
                                                        </div>
                                                        <div class="col-xs-4  text-left">
                                                            <label for="nome">Data Nascimento</label>
                                                        </div>
                                                        <div class="col-xs-8">
                                                            <asp:TextBox ID="TxtFone" class="form-control" MaxLength="15" required="required" onKeyPress="mascara(this, telefone);" placeHolder="Ex: 11 99999-9999 " runat="server"></asp:TextBox>
                                                            <p class="help-block">Insira o seu telefone para retornar o contato.</p>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <asp:TextBox ID="TxtDataNascimento" class="form-control" MaxLength="10" required="required" onKeyPress="mascara(this, data);" placeHolder="Ex: dd/mm/aaaa" runat="server"></asp:TextBox>
                                                            <p class="help-block">Insira sua data de nascimento.</p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-3  text-left">
                                                            <label for="nome">Cep</label>
                                                        </div>
                                                        <div class="col-xs-5  text-left">
                                                            <label for="nome">Endereço</label>
                                                        </div>
                                                        <div class="col-xs-2  text-left">
                                                            <label for="nome">Número</label>
                                                        </div>
                                                        <div class="col-xs-2  text-left">
                                                            <label for="nome">Comple.</label>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <asp:TextBox ID="TxtCep" class="form-control" MaxLength="9" onKeyPress="mascara(this, soNumeros);" required="required" runat="server"></asp:TextBox>
                                                            <p class="help-block">Insira o Cep</p>
                                                        </div>
                                                        <div class="col-xs-5">
                                                            <asp:TextBox ID="TxtEndereco" class="form-control" MaxLength="80" required="required" runat="server"></asp:TextBox>
                                                            <p class="help-block">Insira seu endereço.</p>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <asp:TextBox ID="TxtNumero" class="form-control" MaxLength="6" required="required" runat="server"></asp:TextBox>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <asp:TextBox ID="txtComplemento" class="form-control" MaxLength="10" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-5  text-left">
                                                            <label for="nome">Bairro <span class="required">*</span></label>
                                                        </div>
                                                        <div class="col-xs-5  text-left">
                                                            <label for="nome">Cidade <span class="required">*</span></label>
                                                        </div>
                                                        <div class="col-xs-2  text-left">
                                                            <label for="nome">UF <span class="required">*</span></label>
                                                        </div>
                                                        <div class="col-xs-5">
                                                            <asp:TextBox ID="TxtBairro" class="form-control" required="required" runat="server"></asp:TextBox>
                                                        </div>
                                                        <div class="col-xs-5">
                                                            <asp:TextBox ID="TxtCidade" class="form-control" required="required" runat="server"></asp:TextBox>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <asp:TextBox ID="TxtUf" class="form-control" MaxLength="2" required="required" runat="server"></asp:TextBox>
                                                            <p class="help-block"></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-6  text-left">
                                                            <label for="senha">Senha<span class="required">*</span></label>
                                                        </div>
                                                        <div class="col-xs-6  text-left">
                                                            <label for="senha">Re-Senha<span class="required">*</span></label>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <asp:TextBox ID="TxtSenha" runat="server" TextMode="Password" MaxLength="8" CssClass="form-control" required="required"></asp:TextBox>
                                                            <p class="help-block">
                                                                Informe sua senha
                                                                <br />
                                                                (Max: 8 caracteres.)
                                                            </p>
                                                        </div>

                                                        <div class="col-xs-6">
                                                            <asp:TextBox ID="TextBox1" runat="server" TextMode="Password" MaxLength="8" CssClass="form-control" required="required"></asp:TextBox>
                                                            <p class="help-block">Redigite sua senha</p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <asp:CheckBox ID="ckTerm" runat="server" Text="&nbsp;Eu concordo com o " />
                                                            <a href="javascript:modalRegulamento();">
                                                                <label for="senha">Regulamento da campanha.<span class="required"></span></label></a>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-12 text-left">
                                                            <asp:Button ID="BtnEnviar" class="btn btn-primary btn-outline" OnClick="BtnRegistrarClick" runat="server" Text="Enviar" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-6 col-lg-4 col-lg-offset-1 text-center suporte-text">
                                                    <div class="image">
                                                        <img src="assets/img/logo.png" alt="" width="142" height="215">
                                                    </div>
                                                    <br />
                                                    <div class="links">
                                                        Precisa de ajuda? Entre em contato conosco:
                                                        <a class="btn btn-primary btn-outline" href="tel:+551174702952"><i class="fa fa-whatsapp"></i>Whatsapp - +55 11 97470-2952</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $('#alertaSucesso').hide();
            $('#alertaErro').show();
            window.setTimeout(function () {
                $('#alertaSucesso').hide();
                $('#alertaErro').hide();
            }, 4000);

            function modalRegulamento() {
                $("#modalRegulamento").modal('show');
            }

            function modalErroRegulamento() {
                $("#modalErroRegulamento").modal('show');
            }

            function modalEmail() {
                $("#modalEmail").modal('show');
            }
        </script>
    </form>
</body>
</html>
