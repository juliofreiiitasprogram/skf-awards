﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Contato.aspx.cs" Inherits="WebSite.Contato" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div id="contato" class="col-xs-12 space simple-page">
            <div class="head">
                <div class="title">Entre em Contato</div>
            </div>
            <div class="content">
                <div class="body">
                    <div class="post-content">
                        <%--<div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            Mensagem enviada com sucesso
                        </div>--%>
                        <%--<div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            Ocorreu um erro! Tente novamente mais tarde
                        </div>--%>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div id="AlertaSucesso" class="alert alert-success alert-dismissible alert-fixed" runat="server" visible="false" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Mensagem enviada com sucesso!</strong>
                                </div>
                                <form class="" action="contato.html" method="post">
                                    <div class="form-group">
                                        <div class="col-xs-3 text-right">
                                            <label for="TxtNome">Nome <span class="required">*</span></label>
                                        </div>
                                        <div class="col-xs-9">
                                            <asp:TextBox runat="server" CssClass="form-control" required="required" ID="TxtNome"></asp:TextBox>
                                            <p class="help-block">Insira o seu nome completo.</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-3 text-right">
                                            <label for="nome">E-mail <span class="required">*</span></label>
                                        </div>
                                        <div class="col-xs-9">
                                            <asp:TextBox ID="TxtEmail" class="form-control" required="required" runat="server"></asp:TextBox>
                                            <asp:Label ID="LblEmail" CssClass="message-error" runat="server" Text="Email invalido!!"></asp:Label>
                                            <p class="help-block">Insira o seu e-mail para retornar o contato.</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-3 text-right">
                                            <label for="nome">Telefone</label>
                                        </div>
                                        <div class="col-xs-9">
                                            <asp:TextBox ID="TxtFone" class="form-control" MaxLength="15" required="required" onKeyPress="mascara(this, telefone);" placeHolder="Ex: 11 9999-99999 " runat="server"></asp:TextBox>
                                            <p class="help-block">Insira o seu telefone para retornar o contato.</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-3 text-right">
                                            <label for="nome">Descrição <span class="required">*</span></label>
                                        </div>
                                        <div class="col-xs-9">
                                            <asp:TextBox ID="TxtMensagem" class="form-control" TextMode="MultiLine" required="required" runat="server"></asp:TextBox>
                                            <p class="help-block">Insira a mensagem sobre o assunto que gostaria de falar.</p>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-xs-9 col-xs-offset-3">
                                            <asp:Button ID="BtnEnviar" class="btn btn-primary btn-outline" OnClick="BtnEnviarClick" runat="server" Text="Enviar" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-xs-12 col-md-6 col-lg-4 col-lg-offset-1 text-center suporte-text">
                                <div class="image">
                                    <img src="assets/img/logo.png" alt="" width="142" height="215">
                                </div>
                                <div class="links">
                                    <div class="desc">Você também pode entrar em contato através das informações abaixo:</div>
                                    <a class="btn btn-primary btn-outline" href="tel:+551174702952"><i class="fa fa-whatsapp"></i>Whatsapp - +55 11 97470-2952</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
