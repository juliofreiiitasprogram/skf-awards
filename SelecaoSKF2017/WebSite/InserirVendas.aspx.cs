﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SKFBfl;
using SKFEntity;

namespace WebSite
{
    public partial class InserirVendas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            var userEntity = new UserEntity();

            if (Session["UserName"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                userEntity.UserId = Convert.ToInt32(Session["UserId"]);
                userEntity.UserName = Session["UserName"].ToString();

                BindDistribuidora();
                BindMes();
                BindGrid();

            }
        }

        protected void ddlDistribuidora_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            updDistribuidora.Update();
            updMes.Update();
            updGridInsert.Update();
            BindGrid();

        }

        protected void ddlMes_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            updDistribuidora.Update();
            updMes.Update();
            updGridInsert.Update();
            BindGrid();
        }

        protected void gridInsert_OnRowEditing(object sender, GridViewEditEventArgs e)
        {
            gridInsert.EditIndex = e.NewEditIndex;
            BindGrid();
        }

        protected void gridInsert_OnRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            using (var cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["SKF"].ConnectionString))
            {
                GridViewRow row = gridInsert.Rows[e.RowIndex];

                var userRegisterEntity = new UserEntity();

                userRegisterEntity.UserId = Convert.ToInt32(row.Cells[0].Text);
                userRegisterEntity.MesMeta = Convert.ToInt32(ddlMes.SelectedValue);

                var userRegisterBfl = new UserBfl();

                userRegisterBfl.PesquisarUsuarioMeta(userRegisterEntity);

                if (!userRegisterEntity.Retorno)
                {

                    string myScriptValue = "$('#skfSemMeta').modal('show');";
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "myScriptName", myScriptValue, true);
                    return;
                }
                var cmd = new SqlCommand("spInsereVendas", cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("@UserID", row.Cells[0].Text);
                cmd.Parameters.AddWithValue("@Venda", ((TextBox)row.Cells[2].Controls[0]).Text);
                cmd.Parameters.AddWithValue("@Mes", ddlMes.SelectedValue);
                cmd.Parameters.AddWithValue("@Meta", userRegisterEntity.Meta);
                cmd.Parameters.AddWithValue("@CodeTipo", userRegisterEntity.DistribuidoraCodeTipo.Trim());

                if (cn.State != ConnectionState.Open)
                    cn.Open();

                cmd.ExecuteNonQuery();

                if (cn.State != ConnectionState.Closed)
                    cn.Close();

                gridInsert.EditIndex = -1;
                BindGrid();
            }
        }

        protected void gridInsert_OnRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gridInsert.EditIndex = -1;
            BindGrid();
        }

        private void BindMes()
        {
            ddlMes.DataTextField = "Value";
            ddlMes.DataValueField = "Key";

            ddlMes.DataSource = new Dictionary<int, string>
            {
                //{ 7, "Julho" },
                //{ 8, "Agosto" },
                { 9, "Setembro" },
                { 10, "Outubro" },
                { 11, "Novembro" }
            };

            ddlMes.DataBind();

            ddlMes.SelectedIndex = 0;
        }

        private void BindGrid()
        {
            using (var cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["SKF"].ConnectionString))
            {


                var cmd = new SqlCommand("SELECT ur.UserID, ur.UserName, ISNULL(vm.Vendas, 0) AS 'Vendas' " + Environment.NewLine +
                                         "FROM USER_REGISTER ur" + Environment.NewLine +
                                         "LEFT JOIN SkfVendasMeta vm" + Environment.NewLine +
                                         "ON ur.UserID = vm.UserID" + Environment.NewLine +
                                         "AND vm.Mes = " + ddlMes.SelectedValue + Environment.NewLine +
                                         "WHERE ur.UserDistribuidora = " + ddlDistribuidora.SelectedValue + Environment.NewLine +
                                          " AND ur.UserStatus = 'A'" +
                                         "ORDER BY ur.UserName ASC", cn) { CommandType = CommandType.Text };

                if (cn.State != ConnectionState.Open)
                    cn.Open();

                var dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                gridInsert.DataSource = dt;
                gridInsert.DataBind();

                if (cn.State != ConnectionState.Closed)
                    cn.Close();
            }
        }

        private void BindDistribuidora()
        {
            using (var cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["SKF"].ConnectionString))
            {
                var cmd = new SqlCommand("SELECT CodeId, CodeNome FROM SKFDistribuidora ORDER BY CodeNome", cn) { CommandType = CommandType.Text };

                if (cn.State != ConnectionState.Open)
                    cn.Open();

                var dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                ddlDistribuidora.DataTextField = "CodeNome";
                ddlDistribuidora.DataValueField = "CodeId";

                ddlDistribuidora.DataSource = dt;
                ddlDistribuidora.DataBind();

                if (cn.State != ConnectionState.Closed)
                    cn.Close();
            }

            ddlDistribuidora.SelectedIndex = 0;
        }
    }
}