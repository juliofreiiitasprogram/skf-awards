﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdminEntity;

namespace AdminData
{
    public class NewsData : Contexto
    {
        public NewsEntity InserirNoticia(NewsEntity newsEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append(
                "INSERT INTO SKFNoticias (" +
                "Titulo, " +
                "SubTitulo, " +
                "Data, " +
                "Hora, " +
                "Texto, " +
                "Autor, " +
                "imagem) values (" +
                "'" + newsEntity.Titulo + "', " +
                "'" + newsEntity.SubTitulo + "', " +
                "'" + newsEntity.Data + "', " +
                "'" + newsEntity.Hora + "', " +
                "'" + newsEntity.Texto + "', " +
                "'" + newsEntity.Autor + "', " +
                "'" + newsEntity.imagem + "')"
                );
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();

                newsEntity.Retorno = true;
                return newsEntity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }
        }
        public List<NewsEntity> ListarNoticias(NewsEntity newEntity)
        {
            var listarNews = new List<NewsEntity>();

            Cnn().Open();
            var cmd = new SqlCommand();
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;
            SqlDataReader dr = null;

            sb.Append("SELECT * FROM SKFNoticias");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var newsEntity = new NewsEntity();

                    newsEntity.NewsID = Convert.ToInt32(dr["NewsID"]);
                    newsEntity.Titulo = dr["Titulo"].ToString();
                    newsEntity.SubTitulo = dr["SubTitulo"].ToString();
                    newsEntity.Data = dr["Data"].ToString();
                    newsEntity.Hora = dr["Hora"].ToString();
                    newsEntity.Texto = dr["Texto"].ToString();
                    newsEntity.imagem = dr["imagem"].ToString();
                    newsEntity.Autor = dr["Autor"].ToString();

                    listarNews.Add(newsEntity);
                }
                return listarNews;
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}
