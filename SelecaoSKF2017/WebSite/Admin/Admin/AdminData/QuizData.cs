﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdminEntity;
using System.Data.SqlClient;
using System.Data;

namespace AdminData
{
    public class QuizData : Contexto
    {
        public QuizEntity InserirQuizQuestao(QuizEntity quizEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append(
                "INSERT INTO QUIZ_Questao (" +
                "Questao, " +
                "DataQuestao, " +
                "TipoEmpresa, " +
                "numberQuestion) values (" +
                "'" + quizEntity.Questao + "', " +
                "'" + quizEntity.DataQuestao + "', " +
                "'" + quizEntity.TipoEmpresa + "', " +
                "'" + quizEntity.numberQuestion + "')"
                );
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();

                return quizEntity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }
        }
        public QuizEntity InserirQuizAlternativa(QuizEntity quizEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append(
                "INSERT INTO QUIZ_Alternativa (" +
                "QuestaoID, " +
                "Alternativa, " +
                "Certo) values (" +
                "'" + quizEntity.QuestaoID + "', " +
                "'" + quizEntity.Alternativa + "', " +
                "'" + quizEntity.Certo + "')"
                );
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();

                return quizEntity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }
        }

        public List<QuizEntity> VerificadorQuiz(QuizEntity quizEntity)
        {
            var listarQuiz = new List<QuizEntity>();

            Cnn().Open();
            var cmd = new SqlCommand();
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;
            SqlDataReader dr = null;

            sb.Append("SELECT * FROM QUIZ_Questao where TipoEmpresa = '" + quizEntity.TipoEmpresa + "'");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var quiEntity = new QuizEntity();

                    quiEntity.QuestaoID = Convert.ToInt32(dr["QuestaoID"]);
                    quiEntity.Questao = dr["Questao"].ToString();
                    quiEntity.DataQuestao = dr["DataQuestao"].ToString();
                    quiEntity.TipoEmpresa = dr["TipoEmpresa"].ToString();
                    quiEntity.numberQuestion = dr["numberQuestion"].ToString();

                    listarQuiz.Add(quiEntity);
                }
                return listarQuiz;
            }
            catch (Exception)
            {
                return null;
            }

        }
        public QuizEntity VerificadorQuizAgain(QuizEntity quizEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append("SELECT * FROM QUIZ_Questao where TipoEmpresa = '" + quizEntity.TipoEmpresa + "' AND numberQuestion = '" + quizEntity.numberQuestion + "'");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    quizEntity.QuestaoID = Convert.ToInt32(dr["QuestaoID"]);
                    quizEntity.Questao = dr["Questao"].ToString();
                    quizEntity.DataQuestao = dr["DataQuestao"].ToString();
                    quizEntity.TipoEmpresa = dr["TipoEmpresa"].ToString();
                    quizEntity.numberQuestion = dr["numberQuestion"].ToString();
                }
                else
                {
                    quizEntity.QuestaoID = 0;
                    quizEntity.Questao = "";
                }
                return quizEntity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }

        }
        
    }
}
