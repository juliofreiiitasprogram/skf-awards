﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdminEntity;

namespace AdminData
{
    public class UserData : Contexto
    {
        public List<UserEntity> ListarAtivos(UserEntity userEntity)
        {
            var listarAtivos = new List<UserEntity>();

            Cnn().Open();
            var cmd = new SqlCommand();
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;
            SqlDataReader dr = null;

            sb.Append("SELECT a.UserID" +
                          " ,a.UserName" +
                          " ,a.UserMail" +
                          " ,a.UserStatus" +
                          " ,a.UserTelefone" +
                          " ,b.CodeNome" +
                      " FROM USER_REGISTER a" +
                      " JOIN SKFDistribuidora b" +
                      " ON a.UserDistribuidora = b.CodeID" +
                      " WHERE a.UserStatus = 'A'" +
                      " ORDER BY b.CodeNome");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var usersEntity = new UserEntity();

                    usersEntity.UserID = Convert.ToInt32(dr["UserID"]);
                    usersEntity.UserName = dr["UserName"].ToString();
                    usersEntity.UserMail = dr["UserMail"].ToString();
                    usersEntity.UserStatus = dr["UserStatus"].ToString();
                    usersEntity.UserTelefone = dr["UserTelefone"].ToString();
                    usersEntity.CodeNome = dr["CodeNome"].ToString();

                    listarAtivos.Add(usersEntity);
                }
                return listarAtivos;
            }
            catch (Exception)
            {
                return null;
            }

        }
        public List<UserEntity> ListarAtivosPorSegmentos(UserEntity userEntity)
        {
            var listarAtivos = new List<UserEntity>();

            Cnn().Open();
            var cmd = new SqlCommand();
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;
            SqlDataReader dr = null;

            sb.Append("SELECT a.UserID" +
                          " ,a.UserName" +
                          " ,a.UserMail" +
                          " ,a.UserStatus" +
                          " ,a.UserTelefone" +
                          " ,b.CodeNome" +
                      " FROM USER_REGISTER a" +
                      " JOIN SKFDistribuidora b" +
                      " ON a.UserDistribuidora = b.CodeID" +
                      " WHERE a.UserStatus = 'A' AND b.CodeTipo = '" + userEntity.CodeNome + "'" + 
                      " ORDER BY b.CodeNome");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var usersEntity = new UserEntity();

                    usersEntity.UserID = Convert.ToInt32(dr["UserID"]);
                    usersEntity.UserName = dr["UserName"].ToString();
                    usersEntity.UserMail = dr["UserMail"].ToString();
                    usersEntity.UserStatus = dr["UserStatus"].ToString();
                    usersEntity.UserTelefone = dr["UserTelefone"].ToString();
                    usersEntity.CodeNome = dr["CodeNome"].ToString();

                    listarAtivos.Add(usersEntity);
                }
                return listarAtivos;
            }
            catch (Exception)
            {
                return null;
            }

        }
        public List<UserEntity> ListarAtivosPorNome(UserEntity userEntity)
        {
            var listarAtivos = new List<UserEntity>();

            Cnn().Open();
            var cmd = new SqlCommand();
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;
            SqlDataReader dr = null;

            sb.Append("SELECT a.UserID" +
                          " ,a.UserName" +
                          " ,a.UserMail" +
                          " ,a.UserStatus" +
                          " ,a.UserTelefone" +
                          " ,b.CodeNome" +
                      " FROM USER_REGISTER a" +
                      " JOIN SKFDistribuidora b" +
                      " ON a.UserDistribuidora = b.CodeID" +
                      " WHERE a.UserStatus = 'A' AND a.UserName like '%" + userEntity.CodeNome + "%'" +
                      " ORDER BY b.CodeNome");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var usersEntity = new UserEntity();

                    usersEntity.UserID = Convert.ToInt32(dr["UserID"]);
                    usersEntity.UserName = dr["UserName"].ToString();
                    usersEntity.UserMail = dr["UserMail"].ToString();
                    usersEntity.UserStatus = dr["UserStatus"].ToString();
                    usersEntity.UserTelefone = dr["UserTelefone"].ToString();
                    usersEntity.CodeNome = dr["CodeNome"].ToString();

                    listarAtivos.Add(usersEntity);
                }
                return listarAtivos;
            }
            catch (Exception)
            {
                return null;
            }

        }
        public UserEntity PesquisarUsuario(UserEntity userEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append("SELECT * FROM USER_REGISTER WHERE UserMail = " + "'" + userEntity.UserMail.Trim() + "'" + " AND UserPassword = " + "'" + userEntity.UserPassword.Trim() + "' AND UserPerfil = 1");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    userEntity.UserName = dr["UserName"].ToString();
                    userEntity.UserMail = dr["UserMail"].ToString();
                    userEntity.UserTelefone = dr["UserTelefone"].ToString();
                    userEntity.UserStatus = dr["UserStatus"].ToString();
                }
                else
                {
                    userEntity.UserMail = "";
                    userEntity.UserPassword = "";
                }
                return userEntity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }

        }
    }
}
