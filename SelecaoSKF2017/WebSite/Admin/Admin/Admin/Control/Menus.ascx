﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Menus.ascx.cs" Inherits="Admin.Control.Menus" %>

<div class="side">
    <div class="title collapsed" data-toggle="collapse" data-target="#side-content" aria-expanded="true">
        <div class="icon inline-block">
            <i class="fa fa-newspaper-o"></i>
        </div>
        <div class="text inline-block">
            Notícias
        </div>
    </div>
    <div class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#side-content" aria-expanded="false">
                <span>Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="side-content" class="collapse navbar-collapse in">
            <ul class="nav navbar-nav">
                <li><a href="../Admin/NewsAdd.aspx"><i class="fa fa-plus-circle"></i><span class="link">Adicionar</span></a></li>
                <li><a href="../Admin/NewsEdit.aspx"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="link">Editar</span></a></li>
                <li><a href="../Admin/NewsView.aspx"><i class="fa fa-eye"></i><span class="link">Visualizar</span></a></li>
                <!--<li><a href="#"><i class="fa fa-bookmark-o"></i><span class="link"> Adicionar</span></a></li>-->
            </ul>
        </div>
    </div>
</div>

<div class="side">
    <div class="title collapsed" data-toggle="collapse" data-target="#side-quiz" aria-expanded="true">
        <div class="icon inline-block">
            <i class="fa fa-gamepad"></i>
        </div>
        <div class="text inline-block">
            Quiz
        </div>
    </div>
    <div class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#side-quiz" aria-expanded="false">
                <span>Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="side-quiz" class="collapse navbar-collapse in">
            <ul class="nav navbar-nav">
                <li><a href="../Admin/QuizAdd.aspx"><i class="fa fa-plus-circle"></i><span class="link">Adicionar</span></a></li>
                <li><a href="../Admin/QuizEdit.aspx"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="link">Editar</span></a></li>
                <li><a href="../Admin/QuizView.aspx"><i class="fa fa-eye"></i><span class="link">Visualizar</span></a></li>
                <!--<li><a href="#"><i class="fa fa-bookmark-o"></i><span class="link"> Adicionar</span></a></li>-->
            </ul>
        </div>
    </div>
</div>

<div class="side">
    <div class="title collapsed" data-toggle="collapse" data-target="#side-user" aria-expanded="true">
        <div class="icon inline-block">
            <i class="fa fa-user" aria-hidden="true"></i>
        </div>
        <div class="text inline-block">
            Usuários
        </div>
    </div>
    <div class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#side-user" aria-expanded="false">
                <span>Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="side-user" class="collapse navbar-collapse in">
            <ul class="nav navbar-nav">
                <li><a href="../Admin/UserAtive.aspx"><i class="fa fa-thumbs-up"></i><span class="link">Ativos</span></a></li>
                <!--<li><a href="../Admin/NewsEdit.aspx"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="link">Editar</span></a></li>
                <li><a href="../Admin/NewsView.aspx"><i class="fa fa-eye"></i><span class="link">Visualizar</span></a></li>
                <li><a href="#"><i class="fa fa-bookmark-o"></i><span class="link"> Adicionar</span></a></li>-->
            </ul>
        </div>
    </div>
</div>
