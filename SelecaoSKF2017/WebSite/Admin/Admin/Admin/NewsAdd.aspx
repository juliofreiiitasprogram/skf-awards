﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" ValidateRequest="false" AutoEventWireup="true" CodeBehind="NewsAdd.aspx.cs" Inherits="Admin.NewsAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
    <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML"></script>
    <link href="assets/css/style_editor.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div runat="server" id="sucesso" visible="false">
        <div class="text-success"><i class="fa fa-check"></i>Notícia gravada com sucesso!</div>
    </div>
    <h1>Adicionar notícia</h1>
    <asp:TextBox ID="txtTitulo" runat="server" class="form-control" placeholder="Título"></asp:TextBox>
    <br />
    <asp:TextBox ID="txtSubTitulo" runat="server" class="form-control" placeholder="SubTítulo"></asp:TextBox>
    <br />
    <asp:TextBox ID="mytextarea" name="mytextarea" TextMode="multiline" Columns="50" Rows="5" runat="server" Text="Digite o texto aqui" />
    <br />
    <p>Escolha uma foto para a notícia</p>
    <%--<input id="FileUp" type="file" runat="server"/>--%>
    <asp:FileUpload ID="FileUp" runat="server" type="file" class="btn-file" />
    <script>
        tinymce.init({
            selector: '#ContentPlaceHolder1_mytextarea'
        });
    </script>
    <br />
    <asp:Button ID="btnEnviar" runat="server" class="btn btn-primary" Text="Enviar" OnClick="btnEnviar_Click" />

</asp:Content>
