﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminBfl;
using AdminEntity;
using System.Web.UI.HtmlControls;

namespace Admin
{
    public partial class UserAtive : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userBfl = new UserBfl();
            var userEntity = new UserEntity();

            List<UserEntity> listarAtivos = userBfl.ListarAtivos(userEntity);

            if (listarAtivos.Count == 0)
            {

            }
            else
            {
                for (int i = 0; i < listarAtivos.Count; i++)
                {
                    var listAti = listarAtivos[i];


                    HtmlGenericControl table = new HtmlGenericControl();
                    table.InnerHtml = " <tr>" +
                                        "<td>#" + listAti.UserID + "</td>" +
                                        "<td>" + listAti.UserName + "</td>" +
                                        "<td>" + listAti.UserMail + "</td>" +
                                        "<td>" + listAti.UserTelefone + "</td>" +
                                        "<td>" + listAti.CodeNome + "</td>" +
                                        "<td>" + listAti.UserStatus + "</td>" +
                                    "</tr>";

                    PlaceHolder1.Controls.Add(table);
                }
            }
        }

        protected void ddSegmentacao_SelectedIndexChanged(object sender, EventArgs e)
        {
            PlaceHolder1.Controls.Clear();
            var userEntity = new UserEntity();
            var userBfl = new UserBfl();

            userEntity.CodeNome = ddSegmentacao.SelectedValue;

            List<UserEntity> listarAtivos = userBfl.ListarAtivosPorSegmentos(userEntity);

            if (listarAtivos.Count == 0)
            {

            }
            else
            {
                for (int i = 0; i < listarAtivos.Count; i++)
                {
                    var listAti = listarAtivos[i];


                    HtmlGenericControl table = new HtmlGenericControl();
                    table.InnerHtml = " <tr>" +
                                        "<td>#" + listAti.UserID + "</td>" +
                                        "<td>" + listAti.UserName + "</td>" +
                                        "<td>" + listAti.UserMail + "</td>" +
                                        "<td>" + listAti.UserTelefone + "</td>" +
                                        "<td>" + listAti.CodeNome + "</td>" +
                                        "<td>" + listAti.UserStatus + "</td>" +
                                    "</tr>";

                    PlaceHolder1.Controls.Add(table);
                }
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            PlaceHolder1.Controls.Clear();
            var userEntity = new UserEntity();
            var userBfl = new UserBfl();

            userEntity.CodeNome = txtSearch.Text;

            List<UserEntity> listarAtivos = userBfl.ListarAtivosPorNome(userEntity);

            if (listarAtivos.Count == 0)
            {

            }
            else
            {
                for (int i = 0; i < listarAtivos.Count; i++)
                {
                    var listAti = listarAtivos[i];


                    HtmlGenericControl table = new HtmlGenericControl();
                    table.InnerHtml = " <tr>" +
                                        "<td>#" + listAti.UserID + "</td>" +
                                        "<td>" + listAti.UserName + "</td>" +
                                        "<td>" + listAti.UserMail + "</td>" +
                                        "<td>" + listAti.UserTelefone + "</td>" +
                                        "<td>" + listAti.CodeNome + "</td>" +
                                        "<td>" + listAti.UserStatus + "</td>" +
                                    "</tr>";

                    PlaceHolder1.Controls.Add(table);
                }
            }
        }
    }
}