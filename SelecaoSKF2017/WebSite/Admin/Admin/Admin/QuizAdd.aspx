﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="QuizAdd.aspx.cs" Inherits="Admin.QuizAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div runat="server" id="errorSegmentacao" visible="false">
        <div class="text-warning">
            <p class="bg-danger"><i class="fa fa-ban"></i>Escolha uma segmentação.</p>
        </div>
    </div>
    <h1>Adicionar Pergunta</h1>
    <br />
    <br />
    <p>
        <asp:TextBox ID="txtQuestao" runat="server" class="form-control" placeholder="Digite a pergunta"></asp:TextBox>
    </p>

    <div class="form-group">
        <div class="col-xs-1">
            <input type="radio" id="radioA" runat="server" name="Alternativa" value="A">
        </div>
        <div class="col-xs-11">
            <asp:TextBox ID="txtAlternativaA" runat="server" class="form-control" placeholder="Digite a alternativa A"></asp:TextBox>
        </div>
    </div>
    <br />
    <div class="form-group">
        <div class="col-xs-1">
            <input type="radio" id="radioB" runat="server" name="Alternativa" value="B">
        </div>
        <div class="col-xs-11">
            <asp:TextBox ID="txtAlternativaB" runat="server" class="form-control" placeholder="Digite a alternativa B"></asp:TextBox>
        </div>
    </div>
    <br />
    <div class="form-group">
        <div class="col-xs-1">
            <input type="radio" id="radioC" runat="server" name="Alternativa" value="C">
        </div>
        <div class="col-xs-11">
            <asp:TextBox ID="txtAlternativaC" runat="server" class="form-control" placeholder="Digite a alternativa C"></asp:TextBox>
        </div>
    </div>
    <br />
    <div class="form-group">
        <div class="col-xs-1">
            <input type="radio" id="radioD" runat="server" name="Alternativa" value="D">
        </div>
        <div class="col-xs-11">
            <asp:TextBox ID="txtAlternativaD" runat="server" class="form-control" placeholder="Digite a alternativa D"></asp:TextBox>
        </div>
    </div>
    <br />
    <br />
    <p>Selecione a data desse quiz:</p>
    <div class="col-xs-12 calendar">
        <asp:Calendar ID="clDate" runat="server"></asp:Calendar>
    </div>
    <br />
    <br />
    <br />
    <p>Selecione a segmentação:</p>
    <div class="col-xs-12">
        <asp:DropDownList ID="ddSegmentacao" class="form-control" runat="server">
            <asp:ListItem Value="0">Selecione</asp:ListItem>
            <asp:ListItem Value="Industrial">Industrial</asp:ListItem>
            <asp:ListItem Value="Automotivo">Automotivo</asp:ListItem>
        </asp:DropDownList>
    </div>
    <br />
    <br />
    <br />
    <div class="col-xs-12">
        <asp:Button ID="btnEnviar" class="btn btn-primary" runat="server" OnClick="btnEnviar_Click" Text="Enviar" />
    </div>
</asp:Content>
