﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminEntity;
using AdminBfl;

namespace Admin
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            // Criar uma instancia com do objeto User
            var userEntity = new UserEntity();
            var userBfl = new UserBfl();

            userEntity.UserMail = txtMail.Text;
            userEntity.UserPassword = txtPassword.Text;

            // Efetuar o Login passando os parametros
            userBfl.PesquisarUsuario(userEntity);

            if (userEntity.UserMail != "" && userEntity.UserPassword != "")
            {

                Session["UserID"] = userEntity.UserID;
                Session["UserName"] = userEntity.UserName;
                //Session["UserDistribuidora"] = userEntity.UserDistribuidora;

                Response.Redirect("Default.aspx", false);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "myFunction", "myFunction();", true);
            }
        }
    }
}