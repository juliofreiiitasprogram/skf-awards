﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminBfl;
using AdminEntity;
using System.Web.Services;
using System.IO;
using System.Net;
using System.Text;


namespace Admin
{
    public partial class NewsAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            var User = Session["UserName"];
            var Day = DateTime.Now.Day;
            var Month = DateTime.Now.Month;
            var year = DateTime.Now.Year;
            var Hour = DateTime.Now.Hour;
            var Minute = DateTime.Now.Minute;
            var date = Day + "/" + Month + "/" + year;
            var hours = Hour + ":" + Minute;

            var newsEntity = new NewsEntity();
            var newsBfl = new NewsBfl();

            newsEntity.Titulo = txtTitulo.Text;
            newsEntity.SubTitulo = txtSubTitulo.Text;
            newsEntity.Texto = mytextarea.Text;
            newsEntity.Autor = Convert.ToString(User);

            newsEntity.Data = date;
            newsEntity.Hora = hours;
            newsEntity.imagem = FileUp.PostedFile.FileName;

            if (FileUp.HasFile)
            {
                string arquivoNomeTcc = Path.GetFileName(FileUp.PostedFile.FileName);
                FileUp.PostedFile.SaveAs(Server.MapPath("~/assets/news/") + arquivoNomeTcc);
            }

            newsBfl.InserirNoticia(newsEntity);

            txtTitulo.Text = "";
            txtSubTitulo.Text = "";
            mytextarea.Text = "";

            sucesso.Visible = true;
        }
    }
}