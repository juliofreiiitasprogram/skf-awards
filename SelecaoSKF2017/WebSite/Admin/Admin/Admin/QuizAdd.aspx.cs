﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminEntity;
using AdminBfl;

namespace Admin
{
    public partial class QuizAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            var quizEntity = new QuizEntity();
            var quizBfl = new QuizBfl();

            if (ddSegmentacao.SelectedValue == "0")
            {

            }
            else
            {
                quizEntity.TipoEmpresa = ddSegmentacao.SelectedValue;

                List<QuizEntity> countQuiz = quizBfl.VerificadorQuiz(quizEntity);
                int nbQuestion = countQuiz.Count + 1;


                string day = Convert.ToString(clDate.SelectedDate.Day);
                string month = Convert.ToString(clDate.SelectedDate.Month);
                string date = day + "/" + month + "/2017";

                quizEntity.Questao = txtQuestao.Text;
                quizEntity.TipoEmpresa = ddSegmentacao.SelectedValue;
                quizEntity.numberQuestion = Convert.ToString(nbQuestion);
                quizEntity.DataQuestao = date;

                quizBfl.InserirQuizQuestao(quizEntity);

                quizEntity.TipoEmpresa = ddSegmentacao.SelectedValue;
                quizEntity.numberQuestion = Convert.ToString(nbQuestion);
                quizBfl.VerificadorQuizAgain(quizEntity);

                int questID = quizEntity.QuestaoID;

                if (questID != 0)
                {
                    string vRadio = "";
                    if (radioA.Checked == true)
                    {
                        vRadio = "A";
                    }
                    if (radioB.Checked == true)
                    {
                        vRadio = "B";
                    }
                    if (radioC.Checked == true)
                    {
                        vRadio = "C";
                    }
                    if (radioD.Checked == true)
                    {
                        vRadio = "D";
                    }

                    for (int p = 0; p < 4; p++)
                    {
                        if (p == 0)
                        {
                            if (vRadio == "A")
                            {
                                quizEntity.Certo = "1";
                            }
                            else
                            {
                                quizEntity.Certo = "0";
                            }
                            quizEntity.Alternativa = txtAlternativaA.Text;
                        }
                        if (p == 1)
                        {
                            if (vRadio == "B")
                            {
                                quizEntity.Certo = "1";
                            }
                            else
                            {
                                quizEntity.Certo = "0";
                            }
                            quizEntity.Alternativa = txtAlternativaB.Text;
                        }
                        if (p == 2)
                        {
                            if (vRadio == "C")
                            {
                                quizEntity.Certo = "1";
                            }
                            else
                            {
                                quizEntity.Certo = "0";
                            }
                            quizEntity.Alternativa = txtAlternativaC.Text;
                        }
                        if (p == 3)
                        {
                            if (vRadio == "D")
                            {
                                quizEntity.Certo = "1";
                            }
                            else
                            {
                                quizEntity.Certo = "0";
                            }
                            quizEntity.Alternativa = txtAlternativaD.Text;
                        }
                        quizEntity.QuestaoID = questID;

                        quizBfl.InserirQuizAlternativa(quizEntity);

                        txtQuestao.Text = "";
                        txtAlternativaA.Text = "";
                        txtAlternativaB.Text = "";
                        txtAlternativaC.Text = "";
                        txtAlternativaD.Text = "";
                        ddSegmentacao.SelectedValue = "";
                        radioA.Checked = false;
                        radioB.Checked = false;
                        radioC.Checked = false;
                        radioD.Checked = false;
                    }
                }
                else
                {
                    //Response.Write("error");
                }
            }

        }

    }
}