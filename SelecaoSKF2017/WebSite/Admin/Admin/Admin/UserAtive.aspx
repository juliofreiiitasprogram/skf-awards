﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="UserAtive.aspx.cs" Inherits="Admin.UserAtive" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Usuários Ativos</h1>

    <div class="row">
        <div class="col-xs-4">
            <p>Selecione a segmentação:</p>
            <asp:DropDownList ID="ddSegmentacao" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddSegmentacao_SelectedIndexChanged" runat="server">
                <asp:ListItem Value="0">Selecione</asp:ListItem>
                <asp:ListItem Value="Industrial">Industrial</asp:ListItem>
                <asp:ListItem Value="Automotivo">Automotivo</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="col-xs-4 col-xs-offset-4">
            <p>Digite o nome:</p>
            <div class="input-group">
                <asp:TextBox ID="txtSearch" runat="server" class="form-control" placeholder="Pesquisar..."></asp:TextBox>
                <span class="input-group-btn">
                    <asp:Button ID="btnSearch" runat="server" class="btn btn-default" OnClick="btnSearch_Click" Text="Go!" />
                </span>
            </div>
            <!-- /input-group -->
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <!-- /.row -->
    <br /><br />
    <table border="1" class="table table-striped">
        <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Telefone</th>
            <th>Distribuidora</th>
            <th>Status</th>
        </tr>
        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
    </table>
</asp:Content>
