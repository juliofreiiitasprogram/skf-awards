﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="NewsEdit.aspx.cs" Inherits="Admin.NewsEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Editar notícias</h1>

    <table border="1" class="table table-striped">
        <tr>
            <th>ID</th>
            <th>Titulo</th>
            <th>SubTitulo</th>
            <th>Data / Hora</th>
            <th>Texto</th>
            <th>Autor</th>
            <th>Edit</th>
        </tr>
        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
    </table>
</asp:Content>
