﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminBfl;
using AdminEntity;
using System.Web.UI.HtmlControls;

namespace Admin
{
    public partial class NewsEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var newsBfl = new NewsBfl();
            var newsEntity = new NewsEntity();

            List<NewsEntity> listarNews = newsBfl.ListarNoticias(newsEntity);

            if (listarNews.Count == 0)
            {

            }
            else
            {
                for (int i = 0; i < listarNews.Count; i++)
                {
                    var listNew = listarNews[i];
                    var dateTime = listNew.Data + "-" + listNew.Hora;


                    HtmlGenericControl table = new HtmlGenericControl();
                    table.InnerHtml = " <tr>" +
                                        "<td>#" + listNew.NewsID + "</td>" +
                                        "<td>" + listNew.Titulo + "</td>" +
                                        "<td>" + listNew.SubTitulo + "</td>" +
                                        "<td>" + dateTime + "</td>" +
                                        "<td>" + listNew.Texto +"</td>" +
                                        "<td>" + listNew.Autor + "</td>" +
                                        "<td><a href=\"#\">edit</a><br/><a href=\"#\">delete</a></td>" +
                                    "</tr>";

                    PlaceHolder1.Controls.Add(table);
                }
            }
        }
    }
}