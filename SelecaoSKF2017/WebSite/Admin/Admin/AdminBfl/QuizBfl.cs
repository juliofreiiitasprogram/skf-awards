﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdminData;
using AdminEntity;

namespace AdminBfl
{
    public class QuizBfl
    {
        public QuizEntity InserirQuizQuestao(QuizEntity quizEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var quizData = new QuizData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                quizData.InserirQuizQuestao(quizEntity);
                return quizEntity;
            }
            catch
            {
                return null;
            }
        }
        public QuizEntity InserirQuizAlternativa(QuizEntity quizEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var quizData = new QuizData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                quizData.InserirQuizAlternativa(quizEntity);
                return quizEntity;
            }
            catch
            {
                return null;
            }
        }
        public List<QuizEntity> VerificadorQuiz(QuizEntity quizEntity)
        {
            try
            {
                var quizData = new QuizData();

                return quizData.VerificadorQuiz(quizEntity);

            }
            catch (Exception)
            {
                return null;
            }
        }

        public QuizEntity VerificadorQuizAgain(QuizEntity quizEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var quizData = new QuizData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                quizData.VerificadorQuizAgain(quizEntity);
                return quizEntity;
            }
            catch
            {

                return null;
            }
        }
    }
}
