﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdminEntity;
using AdminData;

namespace AdminBfl
{
    public class NewsBfl
    {
        public NewsEntity InserirNoticia(NewsEntity newsEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var newsData = new NewsData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                newsData.InserirNoticia(newsEntity);
                return newsEntity;
            }
            catch
            {
                return null;
            }
        }
        public List<NewsEntity> ListarNoticias(NewsEntity newEntity)
        {
            try
            {
                var newData = new NewsData();

                return newData.ListarNoticias(newEntity);

            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
