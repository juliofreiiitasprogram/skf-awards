﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdminEntity;
using AdminData;

namespace AdminBfl
{
    public class UserBfl
    {
        public List<UserEntity> ListarAtivos(UserEntity userEntity)
        {
            try
            {
                var userData = new UserData();

                return userData.ListarAtivos(userEntity);

            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<UserEntity> ListarAtivosPorSegmentos(UserEntity userEntity)
        {
            try
            {
                var userData = new UserData();

                return userData.ListarAtivosPorSegmentos(userEntity);

            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<UserEntity> ListarAtivosPorNome(UserEntity userEntity)
        {
            try
            {
                var userData = new UserData();

                return userData.ListarAtivosPorNome(userEntity);

            }
            catch (Exception)
            {
                return null;
            }
        }

        public UserEntity PesquisarUsuario(UserEntity userEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var userData = new UserData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                userData.PesquisarUsuario(userEntity);
                return userEntity;
            }
            catch
            {

                return null;
            }
        }
    }
}
