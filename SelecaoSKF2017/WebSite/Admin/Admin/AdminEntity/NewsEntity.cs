﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminEntity
{
    public class NewsEntity
    {
        public int NewsID { get; set; }
        public string Titulo { get; set; }
        public string Autor { get; set; }
        public string SubTitulo { get; set; }
        public string Data { get; set; }
        public string Hora { get; set; }
        public string Texto { get; set; }
        public string imagem { get; set; }
        public bool Retorno { get; set; }
    }
}
