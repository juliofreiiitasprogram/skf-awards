﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminEntity
{
    public class UserEntity
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public string UserMail { get; set; }
        public string UserStatus { get; set; }
        public string UserTelefone { get; set; }
        public string CodeNome { get; set; }
    }
}
