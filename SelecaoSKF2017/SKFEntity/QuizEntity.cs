﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKFEntity
{
    public class QuizEntity
    {
        public int QuestaoID { get; set; }
        public int AlternativaID { get; set; }
        public int UserID { get; set; }
        public string DataQuestao { get; set; }
        public string Questao { get; set; }
        public string Alternativa { get; set; }
        public string numberQuestion { get; set; }
        public string TipoEmpresa { get; set; }
        public string Certo { get; set; }
    }
}
