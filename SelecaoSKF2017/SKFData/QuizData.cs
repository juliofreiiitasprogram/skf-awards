﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKFEntity;

namespace SKFData
{
    public class QuizData : Contexto
    {
        public List<QuizEntity> ListarQuestoes(QuizEntity quizEntity)
        {
            var listarQuestoes = new List<QuizEntity>();

            Cnn().Open();
            var cmd = new SqlCommand();
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;
            SqlDataReader dr = null;

            sb.Append("SELECT * FROM QUIZ_Questao a, QUIZ_Alternativa b where" + 
                     " ((a.QuestaoID = b.QuestaoID)"+
                     " AND a.TipoEmpresa = '"+ quizEntity.TipoEmpresa +"')"+
                     " AND a.DataQuestao = '"+ quizEntity.DataQuestao +"'");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var questaoEntity = new QuizEntity();

                    questaoEntity.QuestaoID = Convert.ToInt32(dr["QuestaoID"]);
                    questaoEntity.AlternativaID = Convert.ToInt32(dr["AlternativaID"]);
                    questaoEntity.DataQuestao = dr["DataQuestao"].ToString();
                    questaoEntity.Questao = dr["Questao"].ToString();
                    questaoEntity.Alternativa = dr["Alternativa"].ToString();
                    questaoEntity.numberQuestion = dr["numberQuestion"].ToString();
                    questaoEntity.TipoEmpresa = dr["TipoEmpresa"].ToString();
                    questaoEntity.Certo = dr["Certo"].ToString();

                    listarQuestoes.Add(questaoEntity);
                }
                return listarQuestoes;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public QuizEntity RegistrarPonto(QuizEntity quizEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append(
                "INSERT INTO QUIZ_Pontos (" +
                "QuestaoID, " +
                "UserID, " +
                "Alternativa) values (" +
                "'" + quizEntity.QuestaoID + "', " +
                "'" + quizEntity.UserID + "', " +
                "'" + quizEntity.Alternativa + "')"
                );
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();

                //quizEntity.Retorno = true;
                return quizEntity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }
        }

        public QuizEntity PesquisarResposta(QuizEntity quizEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append("SELECT a.QuestaoID, a.UserID, b.DataQuestao, b.numberQuestion"+
                     " FROM QUIZ_Pontos a, QUIZ_Questao b" +
                     " where ((a.QuestaoID = b.QuestaoID) AND a.UserID = " + quizEntity.UserID + ") AND b.DataQuestao = '" + quizEntity.DataQuestao + "'");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    quizEntity.QuestaoID = Convert.ToInt32(dr["QuestaoID"]);
                    quizEntity.UserID = Convert.ToInt32(dr["UserID"]);
                    quizEntity.DataQuestao = dr["DataQuestao"].ToString();
                    quizEntity.numberQuestion = dr["numberQuestion"].ToString();
                }
                else
                {
                    quizEntity.QuestaoID = 0;
                    quizEntity.UserID = 0;
                    quizEntity.DataQuestao = "";
                    quizEntity.numberQuestion = "";
                }
                return quizEntity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }

        }
    }
}
