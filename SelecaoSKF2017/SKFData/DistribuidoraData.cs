﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKF.Entity;
using SKFData;

namespace SKF.Data
{
    public class DistribuidoraData : Contexto
    {
        public List<DistribuidoraEntity> ListarDistribuidoras(DistribuidoraEntity distribuidorEntity)
        {
            var usuarioDistribuidor = new List<DistribuidoraEntity>();

            Cnn().Open();
            var cmd = new SqlCommand();
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;
            SqlDataReader dr = null;

            sb.Append("SELECT * FROM SKFDistribuidora " +
                      " WHERE CodeStatus = 'I' ORDER BY CodeNome");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var distribuidoraEntity = new DistribuidoraEntity();

                    distribuidoraEntity.CodeID = Convert.ToInt32(dr["CodeId"]);
                    distribuidoraEntity.CodeNome = dr["CodeNome"].ToString();
                    distribuidoraEntity.CodeStatus = dr["CodeStatus"].ToString();
                    distribuidoraEntity.CodeNumero = dr["CodeNumero"].ToString();

                    usuarioDistribuidor.Add(distribuidoraEntity);
                }
                return usuarioDistribuidor;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public DistribuidoraEntity DadosDistribuidora(DistribuidoraEntity distribuidorEntity)
        {
            var usuarioDistribuidor = new List<DistribuidoraEntity>();

            Cnn().Open();
            var cmd = new SqlCommand();
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;
            SqlDataReader dr = null;

            sb.Append("SELECT * FROM SKFDistribuidora " +
                      " WHERE CodeStatus = 'I' AND CodeID = '" + distribuidorEntity.CodeID + "' ORDER BY CodeNome");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                dr.Read();
                    
                var distribuidoraEntity = new DistribuidoraEntity();

                    distribuidoraEntity.CodeID = Convert.ToInt32(dr["CodeId"]);
                    distribuidoraEntity.CodeNome = dr["CodeNome"].ToString();
                    distribuidoraEntity.CodeStatus = dr["CodeStatus"].ToString();
                    distribuidoraEntity.CodeNumero = dr["CodeNumero"].ToString();
                    distribuidoraEntity.MetaJaneiro = Convert.ToInt32(dr["MetaJaneiro"]);
                    distribuidoraEntity.MetaFevereiro = Convert.ToInt32(dr["MetaFevereiro"]);
                    distribuidoraEntity.MetaMarco = Convert.ToInt32(dr["MetaMarco"]);
                    distribuidoraEntity.MetaAbril = Convert.ToInt32(dr["MetaAbril"]);
                    distribuidoraEntity.MetaMaio = Convert.ToInt32(dr["MetaMaio"]);

                    
                    return distribuidoraEntity;
            }
            catch (Exception)
            {
                return null;
            }

        }

       
    }
}
