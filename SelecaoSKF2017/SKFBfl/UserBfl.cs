﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKFData;
using SKFEntity;

namespace SKFBfl
{
    public class UserBfl
    {
        public UserEntity PesquisarUsuario(UserEntity userEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var userData = new UserData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                userData.PesquisarUsuario(userEntity);
                return userEntity;
            }
            catch
            {

                return null;
            }
        }
        public UserEntity PesquisarTipoDistribuidora(UserEntity userEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var userData = new UserData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                userData.PesquisarTipoDistribuidora(userEntity);
                return userEntity;
            }
            catch
            {

                return null;
            }
        }
        public UserEntity VerificadorUsuario(UserEntity userEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var userData = new UserData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                userData.VerificadorUsuario(userEntity);
                return userEntity;
            }
            catch
            {

                return null;
            }
        }
        public UserEntity PesquisarUsuarioExiste(UserEntity userEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var userData = new UserData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                userData.PesquisarUsuarioExiste(userEntity);
                return userEntity;
            }
            catch
            {

                return null;
            }
        }
        public UserEntity PesquisarUsuarioMeta(UserEntity userEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var userData = new UserData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                userData.PesquisarUsuarioMeta(userEntity);
                return userEntity;
            }
            catch
            {

                return null;
            }

        }

        public UserEntity AtualizarCadastro(UserEntity userEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var userData = new UserData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                userData.AtualizarCadastro(userEntity);
                return userEntity;
            }
            catch
            {
                return null;
            }
        }

        public UserEntity CadastrarUsuario(UserEntity userEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var userData = new UserData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                userData.CadastrarUsuario(userEntity);
                return userEntity;
            }
            catch
            {
                return null;
            }
        }
    }
}
