﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKF.Data;
using SKF.Entity;

namespace SKF.Bfl
{
    public class DistribuidoraBfl
    {
        public List<DistribuidoraEntity> ListarDistribuidoras(DistribuidoraEntity distribuidorEntity)
        {
            try
            {
                var distribuidoraData = new DistribuidoraData();

                return distribuidoraData.ListarDistribuidoras(distribuidorEntity);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DistribuidoraEntity DadosDistribuidora(DistribuidoraEntity distribuidorEntity)
        {
            try
            {
                var distribuidoraData = new DistribuidoraData();

                return distribuidoraData.DadosDistribuidora(distribuidorEntity);
            }
            catch (Exception)
            {
                return null;
            }
        }

        
    }
}
