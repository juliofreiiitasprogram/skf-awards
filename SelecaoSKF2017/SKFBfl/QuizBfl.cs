﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKFData;
using SKFEntity;

namespace SKFBfl
{
    public class QuizBfl
    {
        public List<QuizEntity> ListarQuestoes(QuizEntity quizEntity)
        {
            try
            {
                var quizData = new QuizData();

                return quizData.ListarQuestoes(quizEntity);

            }
            catch (Exception)
            {
                return null;
            }
        }
        public QuizEntity RegistrarPonto(QuizEntity quizEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var quizData = new QuizData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                quizData.RegistrarPonto(quizEntity);
                return quizEntity;
            }
            catch
            {
                return null;
            }
        }
        public QuizEntity PesquisarResposta(QuizEntity quizEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var quizData = new QuizData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                quizData.PesquisarResposta(quizEntity);
                return quizEntity;
            }
            catch
            {

                return null;
            }
        }
    }
}
